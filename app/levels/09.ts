import { MCLURE } from "../constants/people";
import { TEAM_PINK, TEAM_ORANGE } from "../constants/senders";
import { CLUE_CAESAR_1, CLUE_AZZA } from "../constants/sprites";

export default {
  cipherList: { },
  clues: [CLUE_CAESAR_1, CLUE_AZZA],
  email: {
    content: `
I knew you wouldn't have any trouble with this. We don't know now which cipher they will use, but I am sure you can figure it out.
Good work finding out about their meeting too. This could be a good opportunity to squash any hopes of a resistance before it gets out of hand.`,
    sender: MCLURE,
    title: "I knew you could do it"
  },
  messages: [{
    full: `
I'm so excited for Peggy's party next week. It's been too long since I've been able to have a night off!`,
    sender: TEAM_PINK,
    text: "So excited for party"
  }, {
    full: `
I can't wait either! It will be so much fun!`,
    sender: TEAM_ORANGE,
    text: "Me too"
  }, {
    full: `
OMG I wonder if Brad will be there.`,
    sender: TEAM_ORANGE,
    text: "will brad be there"
  }]
};