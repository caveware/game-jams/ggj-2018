import { MCLURE } from "../constants/people";
import { TEAM_RED, TEAM_BLUE } from "../constants/senders";
import { CLUE_CAESAR_1 } from "../constants/sprites";

import CaeserCipher from "../ciphers/CaesarCipher";
import NumericKey from "../keyConfigs/NumericKey";

const Cae1 = [[CaeserCipher, new NumericKey(1, 1)]];

export default {
  cipherList: {
    CAE1: [Cae1]
  },
  clues: [CLUE_CAESAR_1],
  email: {
    content: `
Good job again on deciphering those messages.
We need to find out if they are going to leave hiding and come out. It seems they are also stepping up their encipherment, ciphering more of the messages. I'm you wont have any trouble with that though.`,
    sender: MCLURE,
    title: "We need more info"
  },
  messages: [{
    full: `
We have a package ready to show you our plan. We will need to exchange it in person though.`,
    sender: TEAM_RED,
    text: "We have a {<CAE1>package} {<CAE1>ready}"
  }, {
    full: `
Where though? We can't risk you drawing the government to our hideout.`,
    sender: TEAM_BLUE,
    text: "{<CAE1>Where} though"
  }, {
    full: `
Or worse, drawing us into a trap. We are weak enough already.`,
    sender: TEAM_BLUE,
    text: "Or {<CAE1>worse}, a {<CAE1>trap}"
  }, ]
};