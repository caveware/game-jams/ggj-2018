import { MCLURE } from "../constants/people";
import { TEAM_BLUE, TEAM_RED } from "../constants/senders";
import { CLUE_CAESAR_1 } from "../constants/sprites";

import CaeserCipher from "../ciphers/CaesarCipher";
import NumericKey from "../keyConfigs/NumericKey";

const Cae1 = [[CaeserCipher, new NumericKey(1, 1)]];

export default {
  cipherList: {
    CAE1: [Cae1]
  },
  clues: [CLUE_CAESAR_1],
  email: {
    content: `
It would appear that the rebels have realised that we may be listening in to their conversations. They have started enciphering the messages. We have already been able to remove the outer layer of encryption, but some of the words don't seem right.
If I had to have a guess, it looks almost like a Casear Cipher. I've added a note to your corkboard with the details.`,
    sender: MCLURE,
    title: "The First Cipher"
  },
  messages: [{
    full: `
They may be listening. Even if you aren't them, we have to be careful.`,
    sender: TEAM_RED,
    text: "They may be {<CAE1>listening}"
  }, {
    full: `
You are right. Are you willing to join us?`,
    sender: TEAM_BLUE,
    text: "You are {<CAE1>right}"
  }, {
    full: `
I am still suspicious. Let me think about it.`,
    sender: TEAM_BLUE,
    text: "I am still {<CAE1>suspicious}"
  }]
};