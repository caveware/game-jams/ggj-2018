import { MCLURE } from "../constants/people";
import { TEAM_RED, TEAM_BLUE } from "../constants/senders";
import { CLUE_AZZA, CLUE_CAESAR_1, CLUE_SUBST } from "../constants/sprites";

import AZZACipher from "../ciphers/AZZACipher";
import CaeserCipher from "../ciphers/CaesarCipher";
import SubstitutionCipher from "../ciphers/SubstitutionCipher";

import StringKey from "../keyConfigs/StringKey";
import NumericKey from "../keyConfigs/NumericKey";
import StaticStringKey from "../keyConfigs/StaticStringKey";

const Cae1 = [[CaeserCipher, new NumericKey(1, 1)]];
const Azza = [AZZACipher];
const Subst = [[SubstitutionCipher, new StringKey(3, 6), true]];


export default {
  cipherList: {
    RAND: [Azza, Cae1, Subst]
  },
  outputCipher: new SubstitutionCipher(new StaticStringKey("Interdiction")),
  clues: [CLUE_CAESAR_1, CLUE_AZZA, CLUE_SUBST],
  email: {
    content: `
I can't believe that they got past us again. Everyone involved must report to me today.
Everyone must up their game starting today. If they get away again, there will be consequences.
For today, use key "Interdiction" for any transcriptions.`,
    sender: MCLURE,
    title: "Again??"
  },
  messages: [{
    full: `
Was there anything useful in what we gave you?`,
    sender: TEAM_BLUE,
    text: "Was {<RAND>anything useful}"
  }, {
    full: `
Yes. Thankyou for coming on such short notice.`,
    sender: TEAM_RED,
    text: "Yes {<RAND>thankyou}"
  }, {
    full: `
We will definitely be able to use this.`,
    sender: TEAM_RED,
    text: "This is {<RAND>definitely useful}"
  }]
};