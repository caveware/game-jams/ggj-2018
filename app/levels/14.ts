import { MCLURE } from "../constants/people";
import { TEAM_ORANGE, TEAM_PINK } from "../constants/senders";
import { CLUE_AZZA, CLUE_CAESAR_1, CLUE_SUBST } from "../constants/sprites";

export default {
  cipherList: { },
  clues: [CLUE_CAESAR_1, CLUE_AZZA, CLUE_SUBST],
  email: {
    content: `
There has been a lower volume of messages coming through recently. Don't be surprised if you have a day without any.`,
    sender: MCLURE,
    title: "Less messages than usual"
  },
  messages: [{
    full: `
What are you going to wear?`,
    sender: TEAM_ORANGE,
    text: "what are you going to wear"
  }, {
    full: `
I was thinking that nice red dress I got for my birthday. What do you think?.`,
    sender: TEAM_PINK,
    text: "MY red dress"
  }, {
    full: `
Perfect!`,
    sender: TEAM_ORANGE,
    text: "Perfect"
  }]
};