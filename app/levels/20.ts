import { MCLURE } from "../constants/people";
import { TEAM_GREEN } from "../constants/senders";
import { CLUE_AZZA, CLUE_CAESAR_1, CLUE_SUBST } from "../constants/sprites";

import AZZACipher from "../ciphers/AZZACipher";
import CaeserCipher from "../ciphers/CaesarCipher";
import SubstitutionCipher from "../ciphers/SubstitutionCipher";

import StringKey from "../keyConfigs/StringKey";
import NumericKey from "../keyConfigs/NumericKey";

const Cae1 = [[CaeserCipher, new NumericKey(1, 1)]];
const Azza = [AZZACipher];
const Subst = [[SubstitutionCipher, new StringKey(3, 6), true]];


export default {
  cipherList: {
    RAND: [Azza, Cae1, Subst]
  },
  clues: [CLUE_CAESAR_1, CLUE_AZZA, CLUE_SUBST],
  email: {
    content: `
Computer system has been verified to be secured from rebel hacks and as such, your transcriptions don't need to be enciphered anymore.
In other news, a paper has been found at the meeting site with detailed blueprints. R&D are trying to decipher this now.`,
    sender: MCLURE,
    title: "Secure again"
  },
  messages: [{
    full: `
He's still doing well.`,
    sender: TEAM_GREEN,
    text: "{<RAND>Hes} still doing {<RAND>well>}"
  }, {
    full: `
Maybe could've decoded the last meeting message quicker.`,
    sender: TEAM_GREEN,
    text: "Maybe {<RAND>decoded quicker}"
  }, {
    full: `
That's not important right now.`,
    sender: TEAM_GREEN,
    text: "That's {<RAND>not important}"
  }]
};
