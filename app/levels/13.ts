import { MCLURE } from "../constants/people";
import { TEAM_RED, TEAM_BLUE } from "../constants/senders";
import { CLUE_AZZA, CLUE_CAESAR_1, CLUE_SUBST } from "../constants/sprites";

import SubstitutionCipher from "../ciphers/SubstitutionCipher";
import StringKey from "../keyConfigs/StringKey";

const Subst = [[SubstitutionCipher, new StringKey(4, 4), true]];

export default {
  cipherList: {
    SUBST: [Subst]
  },
  clues: [CLUE_CAESAR_1, CLUE_AZZA, CLUE_SUBST],
  email: {
    content: `
I can't believe that someone I hand picked for the response team could have been irresponsible enough to be spotted by the rebels. They have been dealt with, but it's infuriating that we were so close, and then nothing.
Just to make things worse, they've upped their encipherment again. It looks like this one is based on a word instead of a straight match. Information is on your pinboard.
If we have any information on a key for the cipher, that will also be put on your pinboard.`,
    sender: MCLURE,
    title: "Why??"
  },
  messages: [{
    full: `
We can't afford a close call like that again.`,
    sender: TEAM_RED,
    text: "can't {<SUBST>afford close calls}"
  }, {
    full: `
We are dropping off the radar for a while.`,
    sender: TEAM_RED,
    text: "{<SUBST>dropping off} the {<SUBST>radar}"
  }]
};