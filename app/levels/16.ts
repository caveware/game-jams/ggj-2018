import { MCLURE } from "../constants/people";
import { TEAM_WHITE } from "../constants/senders";
import { CLUE_AZZA, CLUE_CAESAR_1, CLUE_SUBST } from "../constants/sprites";

import AZZACipher from "../ciphers/AZZACipher";

import CaeserCipher from "../ciphers/CaesarCipher";
import NumericKey from "../keyConfigs/NumericKey";

const Cae1 = [[CaeserCipher, new NumericKey(1, 1)]];
const Azza = [AZZACipher];

export default {
  cipherList: {
    RAND: [Azza, Cae1]
  },
  clues: [CLUE_CAESAR_1, CLUE_AZZA, CLUE_SUBST],
  messages: [{
    full: `
MCLURE`,
    sender: TEAM_WHITE,
    text: "{<RAND>mclure}"
  }, {
    full: `
IS`,
    sender: TEAM_WHITE,
    text: "{<RAND>is}"
  }, {
    full: `
_____`,
    sender: TEAM_WHITE,
    text: "_____"
  }]
};