import { MCLURE } from "../constants/people";
import { TEAM_ORANGE, TEAM_PINK } from "../constants/senders";
import { CLUE_AZZA, CLUE_CAESAR_1, CLUE_SUBST } from "../constants/sprites";

export default {
  cipherList: { },
  clues: [CLUE_CAESAR_1, CLUE_AZZA, CLUE_SUBST],
  email: {
    content: `
Just a reminder that the contents of the office fridge is not "fair game". Please only take things that you put in yourself.
Also, due to "attempted biological warfare", the fridge will be emptied every afternoon. Anything left will be discarded.
Thankyou for your co-operation.`,
    sender: MCLURE,
    title: "Office Fridge"
  },
  messages: []
};