import { MCLURE } from "../constants/people";
import { TEAM_BLUE, TEAM_RED } from "../constants/senders";

export default {
  cipherList: {},
  clues: [],
  email: {
    content: `
Good job on your first day. We knew there were some groups who were plotting against us, but we hadn't been able to get any evidence of it yet.
It would appear that two rebel groups are attempting to work together. Just keep doing what you are doing, and hopefully we can stop them before anything drastic happens.`,
    sender: MCLURE,
    title: "Good start"
  },
  messages: [{
    full: `
We have been out here for years, trying to find anyone who would be able to help us. Where have you been all this time?`,
    sender: TEAM_BLUE,
    text: "Why did it take so long"
  }, {
    full: `
And how do we know you aren't trying to trick us?`,
    sender: TEAM_BLUE,
    text: "And how do we know"
  }, {
    full: `
We know you don't have much time or many resources left. It's now or never isn't it?`,
    sender: TEAM_RED,
    text: "you dont have much left"
  }]
};