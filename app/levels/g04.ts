import { THOMPSON } from "../constants/people";
import { TEAM_RED, TEAM_BLUE } from "../constants/senders";
import { CLUE_AZZA, CLUE_CAESAR_1, CLUE_SUBST, CLUE_NUMER, CLUE_RAIL, CLUE_COLUMN } from "../constants/sprites";

import AZZACipher from "../ciphers/AZZACipher";
import CaeserCipher from "../ciphers/CaesarCipher";
import SubstitutionCipher from "../ciphers/SubstitutionCipher";
import NumericCipher from "../ciphers/NumericCipher";
import RailFenceCipher from "../ciphers/RailFenceCipher";
import ColumnCipher from "../ciphers/ColumnCipher";

import StringKey from "../keyConfigs/StringKey";
import NumericKey from "../keyConfigs/NumericKey";
import StaticStringKey from "../keyConfigs/StaticStringKey";

const Cae1 = [[CaeserCipher, new NumericKey(1, 1)]];
const Azza = [AZZACipher];
const Subst = [[SubstitutionCipher, new StringKey(3, 9), true]];
const Rail = [[RailFenceCipher, new NumericKey(3, 6), true]];
const Col = [[ColumnCipher, new StringKey(3, 6), true]];
const Numer = [NumericCipher];

const Cae1Numer = [[CaeserCipher, new NumericKey(1, 1)], NumericCipher];
const AzzaNumer = [AZZACipher, NumericCipher];
const SubstNumer = [[SubstitutionCipher, new StringKey(3, 9), true], NumericCipher];
const RailNumer = [[RailFenceCipher, new NumericKey(3, 6), true], NumericCipher];
const ColNumer = [[ColumnCipher, new StringKey(3, 6), true], NumericCipher];

export default {
  cipherList: {
    AZZA: [Azza],
    CAE1: [Cae1],
    RAIL: [Rail],
    COL: [Col],
    RAND: [Rail, Subst, Col],
    NUMER: [Numer],
    RAILNUMER: [RailNumer],
    COLNUMER: [ColNumer],
    RANDNUMER: [RailNumer, SubstNumer, ColNumer],
    RANDONUMER: [Rail, Subst, Col, RailNumer, SubstNumer, ColNumer],
  },
  clues: [CLUE_CAESAR_1, CLUE_AZZA, CLUE_SUBST, CLUE_NUMER, CLUE_RAIL, CLUE_COLUMN],
  email: {
    content: `
We have to figure our what they are up to.
This is another new cipher. Our spy delivered a note to us earlier, that I can only assume applies to it.
Good luck.`,
    sender: THOMPSON,
    title: "Another new cipher"
  },
  messages: [{
    full: `
We have found a weakpoint. It will take another day or two to fully charge though.`,
    sender: TEAM_RED,
    text: "{<COL>We have found a weakpoint}"
  }, {
    full: `
Hopefully the amount of power we are drawing won't draw too much attention.`,
    sender: TEAM_BLUE,
    text: "{<COLNUMER>Too much power}"
  }]
};