import { MCLURE } from "../constants/people";
import { TEAM_WHITE } from "../constants/senders";
import { CLUE_AZZA, CLUE_CAESAR_1, CLUE_SUBST, CLUE_NUMER } from "../constants/sprites";

import AZZACipher from "../ciphers/AZZACipher";
import CaeserCipher from "../ciphers/CaesarCipher";
import SubstitutionCipher from "../ciphers/SubstitutionCipher";
import NumericCipher from "../ciphers/NumericCipher";

import StringKey from "../keyConfigs/StringKey";
import NumericKey from "../keyConfigs/NumericKey";
import StaticStringKey from "../keyConfigs/StaticStringKey";

const Cae1 = [[CaeserCipher, new NumericKey(1, 1)]];
const Azza = [AZZACipher];
const Subst = [[SubstitutionCipher, new StringKey(3, 9), true]];
const Numer = [NumericCipher];

const Cae1Numer = [[CaeserCipher, new NumericKey(1, 1)], NumericCipher];
const AzzaNumer = [AZZACipher, NumericCipher];
const SubstNumer = [[SubstitutionCipher, new StringKey(3, 9), true], NumericCipher];

export default {
  cipherList: {
    RAND: [Azza, Cae1],
  },
  clues: [CLUE_CAESAR_1, CLUE_AZZA, CLUE_SUBST, CLUE_NUMER],
  email: {
    content: `
Even if these rebels did have some move to make against us, it would be treated as an act against Utopia. A declaration of war.
A war that we outclass them in in every way. A war we will dedicate ourselves to if only to prove a point.`,
    sender: MCLURE,
    title: "War"
  },
  messages: [{
    full: `
STUBBORN...`,
    sender: TEAM_WHITE,
    text: "{<RAND>stubborn}"
  }, {
    full: `
DANGEROUS...`,
    sender: TEAM_WHITE,
    text: "{<RAND>dangerous}"
  }, {
    full: `
WARMONGER...`,
    sender: TEAM_WHITE,
    text: "{<RAND>warmonger}"
  }]
};