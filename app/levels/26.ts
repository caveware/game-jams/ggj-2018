import { MCLURE } from "../constants/people";
import { TEAM_WHITE } from "../constants/senders";
import { CLUE_AZZA, CLUE_CAESAR_1, CLUE_SUBST, CLUE_NUMER } from "../constants/sprites";

import AZZACipher from "../ciphers/AZZACipher";
import CaeserCipher from "../ciphers/CaesarCipher";
import SubstitutionCipher from "../ciphers/SubstitutionCipher";
import NumericCipher from "../ciphers/NumericCipher";

import StringKey from "../keyConfigs/StringKey";
import NumericKey from "../keyConfigs/NumericKey";
import StaticStringKey from "../keyConfigs/StaticStringKey";

const Cae1 = [[CaeserCipher, new NumericKey(1, 1)]];
const Azza = [AZZACipher];
const Subst = [[SubstitutionCipher, new StringKey(3, 9), true]];
const Numer = [NumericCipher];

const Cae1Numer = [[CaeserCipher, new NumericKey(1, 1)], NumericCipher];
const AzzaNumer = [AZZACipher, NumericCipher];
const SubstNumer = [[SubstitutionCipher, new StringKey(3, 9), true], NumericCipher];

export default {
  cipherList: {
    RAND: [Azza, Cae1],
  },
  clues: [CLUE_CAESAR_1, CLUE_AZZA, CLUE_SUBST, CLUE_NUMER],
  email: {
    content: `
We will not give anything. We are power. We are greatness. We will not stoop to the level of these terrorists by even acknowledging their threats.
They are empty threats. We will not negotiate. We will overrule them.`,
    sender: MCLURE,
    title: "Empty Threats"
  },
  messages: [{
    full: `
HONOURABLE...`,
    sender: TEAM_WHITE,
    text: "{<RAND>HONOURABLE}"
  }, {
    full: `
PATRIOTIC...`,
    sender: TEAM_WHITE,
    text: "{<RAND>patriotic}"
  }, {
    full: `
HEROIC...`,
    sender: TEAM_WHITE,
    text: "{<RAND>heroic}"
  }]
};