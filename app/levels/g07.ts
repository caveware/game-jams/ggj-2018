import { THOMPSON } from "../constants/people";
import { CREDITS_STATE } from "../constants/states";

export default {
  cipherList: { },
  clues: [],
  email: {
    content: `
Congratulations to all.
Thanks to the diligent work of all of you, the rebels have been disbanded, and Utopia stands, stronger than ever. After the weapon was stopped, Agent Burnell led our troops to the base of the rebels, so we could stop their scheming.
Utopia is safe.
Well done everyone.`,
    sender: THOMPSON,
    title: "Congratulations"
  },
  messages: [],
  nextState: CREDITS_STATE
};
