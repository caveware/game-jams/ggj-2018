import { MCLURE } from "../constants/people";
import { TEAM_GREEN } from "../constants/senders";
import { CLUE_CAESAR_1 } from "../constants/sprites";

import CaeserCipher from "../ciphers/CaesarCipher";
import NumericKey from "../keyConfigs/NumericKey";

const Cae1 = [[CaeserCipher, new NumericKey(1, 1)]];

export default {
  cipherList: {
    CAE1: [Cae1]
  },
  clues: [CLUE_CAESAR_1],
  messages: [{
    full: `
He is doing really well so far. No troubles at all.`,
    sender: TEAM_GREEN,
    text: "He is doing {<CAE1>well}"
  }, {
    full: `
He may be the one we have been looking for.`,
    sender: TEAM_GREEN,
    text: "He may be the {<CAE1>one}"
  }, {
    full: `
I don't know. Only time will tell...`,
    sender: TEAM_GREEN,
    text: "I dont {<CAE1>know}"
  }, ]
};