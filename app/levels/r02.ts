import { GLYNNE } from "../constants/people";
import { TEAM_GREEN } from "../constants/senders";
import { DESK_CORKBOARD_REBEL, CLUE_AZZA, CLUE_CAESAR_1, CLUE_SUBST, CLUE_NUMER, CLUE_RAIL, CLUE_COLUMN } from "../constants/sprites";

import AZZACipher from "../ciphers/AZZACipher";
import CaeserCipher from "../ciphers/CaesarCipher";
import SubstitutionCipher from "../ciphers/SubstitutionCipher";
import NumericCipher from "../ciphers/NumericCipher";
import RailFenceCipher from "../ciphers/RailFenceCipher";
import ColumnCipher from "../ciphers/ColumnCipher";

import StringKey from "../keyConfigs/StringKey";
import NumericKey from "../keyConfigs/NumericKey";
import StaticStringKey from "../keyConfigs/StaticStringKey";

const Cae1 = [[CaeserCipher, new NumericKey(1, 1)]];
const Azza = [AZZACipher];
const Subst = [[SubstitutionCipher, new StringKey(3, 9), true]];
const Rail = [[RailFenceCipher, new NumericKey(3, 6), true]];
const Col = [[ColumnCipher, new StringKey(3, 6), true]];
const Numer = [NumericCipher];

const Cae1Numer = [[CaeserCipher, new NumericKey(1, 1)], NumericCipher];
const AzzaNumer = [AZZACipher, NumericCipher];
const SubstNumer = [[SubstitutionCipher, new StringKey(3, 9), true], NumericCipher];
const RailNumer = [[RailFenceCipher, new NumericKey(3, 6), true], NumericCipher];
const ColNumer = [[ColumnCipher, new StringKey(3, 6), true], NumericCipher];

export default {
  cipherList: {
    AZZA: [Azza],
    CAE1: [Cae1],
    RAIL: [Rail],
    COL: [Col],
    RAND: [Rail, Subst, Col],
    NUMER: [Numer],
    RAILNUMER: [RailNumer],
    COLNUMER: [ColNumer],
    RANDNUMER: [RailNumer, SubstNumer, ColNumer],
    RANDONUMER: [Rail, Subst, Col, RailNumer, SubstNumer, ColNumer],
  },
  clues: [CLUE_CAESAR_1, CLUE_AZZA, CLUE_SUBST, CLUE_NUMER, CLUE_RAIL, CLUE_COLUMN],
  corkboard: DESK_CORKBOARD_REBEL,
  email: {
    content: `
I am Linsday Glynne. I am the, now second-, best decipherer amongst the rebels. While you may be more versed in decipher in general though, nothing beats first hand field experience. Here are my notes on ciphers the government has been known to use recently. You should still have some information on the keys that they were using. It will take a couple of days before they can change the keys, and a couple more before they can change the length, so use that to our advantage until then.`,
    sender: GLYNNE,
    title: "New decipherer, new ciphers"
  },
  messages: [{
    full: `
Burnell is back? She said she saw the new guy at rebel base?`,
    sender: TEAM_GREEN,
    text: "{<COL>{<CAE1>Burnell} is back}"
  }, {
    full: `
I guess she can't stay there if he might recognise her...`,
    sender: TEAM_GREEN,
    text: "{<RAILNUMER>I guess she cant stay}"
  }]
};
