import { DAVIDSON } from "../constants/people";
import { TEAM_PINK, TEAM_ORANGE } from "../constants/senders";
import { CLUE_AZZA, CLUE_CAESAR_1, CLUE_SUBST, CLUE_NUMER } from "../constants/sprites";

import AZZACipher from "../ciphers/AZZACipher";
import CaeserCipher from "../ciphers/CaesarCipher";
import SubstitutionCipher from "../ciphers/SubstitutionCipher";
import NumericCipher from "../ciphers/NumericCipher";

import StringKey from "../keyConfigs/StringKey";
import NumericKey from "../keyConfigs/NumericKey";
import StaticStringKey from "../keyConfigs/StaticStringKey";

const Cae1 = [[CaeserCipher, new NumericKey(1, 1)]];
const Azza = [AZZACipher];
const Subst = [[SubstitutionCipher, new StringKey(3, 9), true]];
const Numer = [NumericCipher];

const Cae1Numer = [[CaeserCipher, new NumericKey(1, 1)], NumericCipher];
const AzzaNumer = [AZZACipher, NumericCipher];
const SubstNumer = [[SubstitutionCipher, new StringKey(3, 9), true], NumericCipher];

export default {
  cipherList: {
    RAND: [Azza, Cae1],
  },
  clues: [CLUE_CAESAR_1, CLUE_AZZA, CLUE_SUBST, CLUE_NUMER],
  email: {
    content: `
We know you were the one deciphering our messages. We could use someone like you on our side. Your target will have much stronger ciphers than we have been able to use, so if you don't know what ciphers may be coming up, you probably won't be much use to us anyway.
If you think you can handle it, you would be contributing to a cause much greater than yourself. We cannot give you long to make your decision, but we hope you make the right one.`,
    sender: DAVIDSON,
    title: "Join Us"
  },
  messages: [{
    full: `
Did you see the news? A govenment officer was killed yesterday?`,
    sender: TEAM_PINK,
    text: "Did you see the news"
  }, {
    full: `
But no body, no witnesses? Suspicious as hell!`,
    sender: TEAM_ORANGE,
    text: "But no evidence"
  }, {
    full: `
There was just that casette tape left at the scene. Sounds pretty gruesome though.`,
    sender: TEAM_PINK,
    text: "Just that tape"
  }]
};
