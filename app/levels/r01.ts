import { DAVIDSON } from "../constants/people";
import { TEAM_GREEN } from "../constants/senders";
import { DESK_CORKBOARD_REBEL, CLUE_AZZA, CLUE_CAESAR_1, CLUE_SUBST, CLUE_NUMER } from "../constants/sprites";

import AZZACipher from "../ciphers/AZZACipher";
import CaeserCipher from "../ciphers/CaesarCipher";
import SubstitutionCipher from "../ciphers/SubstitutionCipher";
import NumericCipher from "../ciphers/NumericCipher";

import StringKey from "../keyConfigs/StringKey";
import NumericKey from "../keyConfigs/NumericKey";
import StaticStringKey from "../keyConfigs/StaticStringKey";

const Cae1 = [[CaeserCipher, new NumericKey(1, 1)]];
const Azza = [AZZACipher];
const Subst = [[SubstitutionCipher, new StringKey(3, 9), true]];
const Numer = [NumericCipher];

const Cae1Numer = [[CaeserCipher, new NumericKey(1, 1)], NumericCipher];
const AzzaNumer = [AZZACipher, NumericCipher];
const SubstNumer = [[SubstitutionCipher, new StringKey(3, 9), true], NumericCipher];

export default {
  cipherList: {
    AZZA: [Azza],
    RAND: [Azza, Cae1, Subst],
    NUMER: [Numer],
    RANDNUMER: [AzzaNumer, Cae1Numer, SubstNumer],
    RANDONUMER: [Azza, Cae1, Subst, AzzaNumer, Cae1Numer, SubstNumer]
  },
  clues: [CLUE_CAESAR_1, CLUE_AZZA, CLUE_SUBST, CLUE_NUMER],
  corkboard: DESK_CORKBOARD_REBEL,
  email: {
    content: `
I'm glad to see you decided to join us, but I'm afraid we don't have time for pleasantries. We have a plan on how we can take down the Govt, but the longer we leave it the longer they have to find a solution. We can explain why we are doing all this later, but since you are here, you must believe that already. The messages you are deciphering here will be more difficult than what you have been deciphering from us, but I'm sure you can do it.`,
    sender: DAVIDSON,
    title: "Welcome to the Uprising."
  },
  messages: [{
    full: `
Where is the new guy? He's only been here a couple of months.`,
    sender: TEAM_GREEN,
    text: "{<RAND>Where is the {<AZZA>new guy}}"
  }, {
    full: `
Is he already missing work? Some people just don't care enough for Utopia.`,
    sender: TEAM_GREEN,
    text: "{<RANDONUMER>already missing work}"
  }]
};
