import { MCLURE } from "../constants/people";
import { TEAM_BLUE } from "../constants/senders";
import { CLUE_AZZA, CLUE_CAESAR_1, DESK_CORKBOARD_REBEL } from "../constants/sprites";

import ColumnCipher from "../ciphers/ColumnCipher";
import StringKey from "../keyConfigs/StringKey";

const ColumnOnly = [[ColumnCipher, new StringKey(3, 3)]];

export default {
  cipherList: {
    COL: [ColumnOnly]
  },
  clues: [ CLUE_AZZA, CLUE_CAESAR_1 ],
  corkboard: DESK_CORKBOARD_REBEL,
  email: {
    content: `
Well fucking hell, you're finally starting.

Good fucking luck.
    `,
    sender: MCLURE,
    title: "Welcome!"
  },
  messages: [{
    full: "Cool",
    sender: TEAM_BLUE,
    text: "{<COL>HELLO_THIS_IS_MCLURE}"
  }]
};