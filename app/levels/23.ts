import { DAVIDSON } from "../constants/people";
import { TEAM_RED, TEAM_BLUE } from "../constants/senders";
import { CLUE_AZZA, CLUE_CAESAR_1, CLUE_SUBST, CLUE_NUMER } from "../constants/sprites";

import AZZACipher from "../ciphers/AZZACipher";
import CaeserCipher from "../ciphers/CaesarCipher";
import SubstitutionCipher from "../ciphers/SubstitutionCipher";
import NumericCipher from "../ciphers/NumericCipher";

import StringKey from "../keyConfigs/StringKey";
import NumericKey from "../keyConfigs/NumericKey";
import StaticStringKey from "../keyConfigs/StaticStringKey";

const Cae1 = [[CaeserCipher, new NumericKey(1, 1)]];
const Azza = [AZZACipher];
const Subst = [[SubstitutionCipher, new StringKey(3, 6), true]];
const Numer = [NumericCipher];

const Cae1Numer = [[CaeserCipher, new NumericKey(1, 1)], NumericCipher];
const AzzaNumer = [AZZACipher, NumericCipher];
const SubstNumer = [[SubstitutionCipher, new StringKey(3, 6), true], NumericCipher];

export default {
  cipherList: {
    RAND: [Azza, Cae1, Subst],
    NUMER: [Numer],
    RANDNUMER: [AzzaNumer, Cae1Numer, SubstNumer],
    RANDONUMER: [Azza, Cae1, Subst, AzzaNumer, Cae1Numer, SubstNumer]
  },
  clues: [CLUE_CAESAR_1, CLUE_AZZA, CLUE_SUBST, CLUE_NUMER],
  email: {
    content: `
McLure. You know what we want. You have one week before our hands are forced.`,
    sender: DAVIDSON,
    title: "It is time"
  },
  messages: [{
    full: `
We have sent the threat. It's time.`,
    sender: TEAM_RED,
    text: "We have {<RANDONUMER>sent} the {<RANDONUMER>threat}"
  }, {
    full: `
In {one week}, we will meet our {goal}, or we will destroy them.`,
    sender: TEAM_RED,
    text: "{<RANDONUMER>One week} left"
  }, {
    full: `
Already? We still have so much preparation to do.`,
    sender: TEAM_BLUE,
    text: "{<RANDONUMER>already}"
  }]
};