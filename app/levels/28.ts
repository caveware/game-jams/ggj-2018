import { MCLURE } from "../constants/people";
import { TEAM_WHITE } from "../constants/senders";
import { CLUE_AZZA, CLUE_CAESAR_1, CLUE_SUBST, CLUE_NUMER } from "../constants/sprites";

import AZZACipher from "../ciphers/AZZACipher";
import CaeserCipher from "../ciphers/CaesarCipher";
import SubstitutionCipher from "../ciphers/SubstitutionCipher";
import NumericCipher from "../ciphers/NumericCipher";

import StringKey from "../keyConfigs/StringKey";
import NumericKey from "../keyConfigs/NumericKey";
import StaticStringKey from "../keyConfigs/StaticStringKey";

const Cae1 = [[CaeserCipher, new NumericKey(1, 1)]];
const Azza = [AZZACipher];
const Subst = [[SubstitutionCipher, new StringKey(3, 9), true]];
const Numer = [NumericCipher];

const Cae1Numer = [[CaeserCipher, new NumericKey(1, 1)], NumericCipher];
const AzzaNumer = [AZZACipher, NumericCipher];
const SubstNumer = [[SubstitutionCipher, new StringKey(3, 9), true], NumericCipher];

export default {
  cipherList: {
    RAND: [Azza, Cae1],
  },
  clues: [CLUE_CAESAR_1, CLUE_AZZA, CLUE_SUBST, CLUE_NUMER],
  email: {
    content: `
But no matter what. We will win. This grand country will always come out on top.
Rebel scum such as these stand no chance against the might of Utopia. We will always stay one step ahead.`,
    sender: MCLURE,
    title: "Victory"
  },
  messages: [{
    full: `
CRUEL...`,
    sender: TEAM_WHITE,
    text: "{<RAND>CRUEL}"
  }, {
    full: `
EVIL...`,
    sender: TEAM_WHITE,
    text: "{<RAND>EVIL}"
  }, {
    full: `
...`,
    sender: TEAM_WHITE,
    text: "___"
  }, {
    full: `
DEAD`,
    sender: TEAM_WHITE,
    text: "DEAD"
  }]
};