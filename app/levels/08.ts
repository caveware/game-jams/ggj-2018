import { MCLURE } from "../constants/people";
import { TEAM_RED, TEAM_BLUE } from "../constants/senders";
import { CLUE_CAESAR_1, CLUE_AZZA } from "../constants/sprites";

import CaeserCipher from "../ciphers/CaesarCipher";
import NumericKey from "../keyConfigs/NumericKey";
import AZZACipher from "../ciphers/AZZACipher";

const Cae1 = [[CaeserCipher, new NumericKey(1, 1)]];
const Azza = [AZZACipher];

export default {
  cipherList: {
    AZZA: [Azza]
  },
  clues: [CLUE_CAESAR_1, CLUE_AZZA],
  email: {
    content: `
It seems that they know we are on to them. They have added a new type of cipher to their messages. I've added a new pin to your pinboard.
Good luck`,
    sender: MCLURE,
    title: "New cipher"
  },
  messages: [{
    full: `
Where is this place you mentioned? It may be our best shot.`,
    sender: TEAM_RED,
    text: "That {<AZZA>place} you {<AZZA>mentioned}"
  }, {
    full: `
There is an abandoned mill to the south of the capital.`,
    sender: TEAM_BLUE,
    text: "Theres an {<AZZA>abandoned mill}"
  }, {
    full: `
We can be there in three days.`,
    sender: TEAM_BLUE,
    text: "Its {<AZZA>three} days away."
  }, {
    full: `
Be there at midday, sharp in three days, then.`,
    sender: TEAM_BLUE,
    text: "Be there at {<AZZA>midday} {<AZZA>sharp}"
  }, {
    full: `
We can't afford to be seen. Make sure you aren't followed.`,
    sender: TEAM_BLUE,
    text: "We cant {<AZZA>afford} to be {<AZZA>seen}"
  }]
};