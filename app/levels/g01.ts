import { THOMPSON } from "../constants/people";
import { TEAM_RED } from "../constants/senders";
import { CLUE_AZZA, CLUE_CAESAR_1, CLUE_SUBST, CLUE_NUMER } from "../constants/sprites";

import AZZACipher from "../ciphers/AZZACipher";
import CaeserCipher from "../ciphers/CaesarCipher";
import SubstitutionCipher from "../ciphers/SubstitutionCipher";
import NumericCipher from "../ciphers/NumericCipher";

import StringKey from "../keyConfigs/StringKey";
import NumericKey from "../keyConfigs/NumericKey";
import StaticStringKey from "../keyConfigs/StaticStringKey";

const Cae1 = [[CaeserCipher, new NumericKey(1, 1)]];
const Azza = [AZZACipher];
const Subst = [[SubstitutionCipher, new StringKey(3, 9), true]];
const Numer = [NumericCipher];

const Cae1Numer = [[CaeserCipher, new NumericKey(1, 1)], NumericCipher];
const AzzaNumer = [AZZACipher, NumericCipher];
const SubstNumer = [[SubstitutionCipher, new StringKey(3, 9), true], NumericCipher];

export default {
  cipherList: {
    RAND: [Azza, Cae1, Subst],
    NUMER: [Numer],
    RANDNUMER: [AzzaNumer, Cae1Numer, SubstNumer],
    RANDONUMER: [Azza, Cae1, Subst, AzzaNumer, Cae1Numer, SubstNumer]
  },
  clues: [CLUE_CAESAR_1, CLUE_AZZA, CLUE_SUBST, CLUE_NUMER],
  email: {
    content: `
We don't know what the rebels are up to, but we must find out.
Keep at it. I've heard great things about you.`,
    sender: THOMPSON,
    title: "We need to know."
  },
  messages: [{
    full: `
The decrypter refused our offer. It is a shame, but it is what it is.`,
    sender: TEAM_RED,
    text: "{<RANDONUMER>He refused our offer}"
  }, {
    full: `
We don't need him anyway. We can still do it.`,
    sender: TEAM_RED,
    text: "{<RANDONUMER>We don't need him anyway}"
  }]
};
