import { MCLURE } from "../constants/people";
import { TEAM_RED } from "../constants/senders";
import { CLUE_AZZA, CLUE_CAESAR_1, CLUE_SUBST } from "../constants/sprites";

import AZZACipher from "../ciphers/AZZACipher";
import CaeserCipher from "../ciphers/CaesarCipher";
import SubstitutionCipher from "../ciphers/SubstitutionCipher";

import StringKey from "../keyConfigs/StringKey";
import NumericKey from "../keyConfigs/NumericKey";
import StaticStringKey from "../keyConfigs/StaticStringKey";

const Cae1 = [[CaeserCipher, new NumericKey(1, 1)]];
const Azza = [AZZACipher];
const Subst = [[SubstitutionCipher, new StringKey(3, 6), true]];


export default {
  cipherList: {
    RAND: [Azza, Cae1, Subst]
  },
  outputCipher: new SubstitutionCipher(new StaticStringKey("Utopian")),
  clues: [CLUE_CAESAR_1, CLUE_AZZA, CLUE_SUBST],
  email: {
    content: `
There have been reports of rebel hacking into our systems.
We believe these are just rumours, but in the interest of security, please encipher any output you may have using the substitution cipher with key "Utopian".
Thankyou.`,
    sender: MCLURE,
    title: "!!Urgent!!"
  },
  messages: [{
    full: `
We are close to from your base. If you come now, we can meet up before they have a chance to respond.`,
    sender: TEAM_RED,
    text: "We are {<RAND>close} to {<RAND>your base}"
  }, {
    full: `
We have hacked into the government systems, and tripped some alarms to throw them off.`,
    sender: TEAM_RED,
    text: "We {<RAND>hacked} the {<RAND>gov network}"
  }, {
    full: `
We don't have much time. Hurry`,
    sender: TEAM_RED,
    text: "We {<RAND>don't} have {<RAND>much time}"
  }]
};