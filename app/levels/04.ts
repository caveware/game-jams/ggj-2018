import { MCLURE } from "../constants/people";
import { TEAM_BLUE, TEAM_RED } from "../constants/senders";
import { CLUE_CAESAR_1 } from "../constants/sprites";

import CaeserCipher from "../ciphers/CaesarCipher";
import NumericKey from "../keyConfigs/NumericKey";

const Cae1 = [[CaeserCipher, new NumericKey(1, 1)]];

export default {
  cipherList: {
    CAE1: [Cae1]
  },
  clues: [CLUE_CAESAR_1],
  email: {
    content: `
Good job deciphering yesterday's messages. Keep up the good work.`,
    sender: MCLURE,
    title: "Keep up the good work"
  },
  messages: [{
    full: `
What can you do to help us?`,
    sender: TEAM_BLUE,
    text: "How can you {<CAE1>help}"
  }, {
    full: `
I can't explain right now, but soon we can show you.`,
    sender: TEAM_RED,
    text: "I cant {<CAE1>explain} right now"
  }, {
    full: `
Give us a few days, we will have something ready.`,
    sender: TEAM_RED,
    text: "Give us a few {<CAE1>days}"
  }, {
    full: `
Don't let us down. We can't deal with more false hope.`,
    sender: TEAM_BLUE,
    text: "Don't let us {<CAE1>down}"
  }]
};