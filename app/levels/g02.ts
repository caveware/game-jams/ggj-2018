import { THOMPSON } from "../constants/people";
import { TEAM_RED, TEAM_BLUE } from "../constants/senders";
import { CLUE_AZZA, CLUE_CAESAR_1, CLUE_SUBST, CLUE_NUMER, CLUE_RAIL } from "../constants/sprites";

import AZZACipher from "../ciphers/AZZACipher";
import CaeserCipher from "../ciphers/CaesarCipher";
import SubstitutionCipher from "../ciphers/SubstitutionCipher";
import NumericCipher from "../ciphers/NumericCipher";
import RailFenceCipher from "../ciphers/RailFenceCipher";

import StringKey from "../keyConfigs/StringKey";
import NumericKey from "../keyConfigs/NumericKey";
import StaticStringKey from "../keyConfigs/StaticStringKey";

const Cae1 = [[CaeserCipher, new NumericKey(1, 1)]];
const Azza = [AZZACipher];
const Subst = [[SubstitutionCipher, new StringKey(3, 9), true]];
const Rail = [[RailFenceCipher, new NumericKey(3, 6), true]];
const Numer = [NumericCipher];

const Cae1Numer = [[CaeserCipher, new NumericKey(1, 1)], NumericCipher];
const AzzaNumer = [AZZACipher, NumericCipher];
const SubstNumer = [[SubstitutionCipher, new StringKey(3, 9), true], NumericCipher];
const RailNumer = [[RailFenceCipher, new NumericKey(3, 6), true], NumericCipher];

export default {
  cipherList: {
    AZZA: [Azza],
    CAE1: [Cae1],
    RAIL: [Rail],
    RAND: [Rail, Subst],
    NUMER: [Numer],
    RAILNUMER: [RailNumer],
    RANDNUMER: [RailNumer, SubstNumer],
    RANDONUMER: [Rail, Subst, RailNumer, SubstNumer]
  },
  clues: [CLUE_CAESAR_1, CLUE_AZZA, CLUE_SUBST, CLUE_NUMER, CLUE_RAIL],
  email: {
    content: `
This is new. I have not seen this cipher before. There was a note left in McLure's desk that might apply, but I can't know for sure.
They must be really getting ready to do something big if they are breaking out these ciphers.`,
    sender: THOMPSON,
    title: "New cipher"
  },
  messages: [{
    full: `
How far away from being ready are you?`,
    sender: TEAM_BLUE,
    text: "{<RAIL>How long do you need}"
  }, {
    full: `
The weapons are being prepared as we speak.`,
    sender: TEAM_RED,
    text: "{<RAIL>We are preparing now}"
  }, {
    full: `
We will be ready before they can stop us.`,
    sender: TEAM_RED,
    text: "{<RAILNUMER>We will be ready}"
  }]
};
