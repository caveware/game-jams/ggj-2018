export default class BaseState extends Phaser.State {
  getDeltaTime (game: Phaser.Game) {
    // Restrict delta time to 15th of a second
    return Math.min(1 / 15, game.time.physicsElapsedMS / 1000);
  }
}
