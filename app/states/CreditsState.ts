import { timeline } from "popmotion";

// Classes
import BaseState from "./BaseState";

// Constants
import { BGM_CREDITS } from "../constants/audio";
import { CREDITS_IMAGE, SPLASH_IMAGE } from "../constants/sprites";
import { SPLASH_STATE } from "../constants/states";

export default class CreditsState extends BaseState {
  create () {
    this.sound.play(BGM_CREDITS);

    const credits = this.add.sprite(0, 0, CREDITS_IMAGE);
    credits.alpha = 0;

    timeline([
      { track: "fadeIn", from: 0, to: 1, duration: 1000 },
      31000,
      { track: "fadeOut", from: 0, to: 1, duration: 1000 }
    ]).start({
      complete: () => {
        this.state.start(SPLASH_STATE);
      },
      update: (value) => {
        credits.alpha = value.fadeIn - value.fadeOut;
      }
    });
  }
}
