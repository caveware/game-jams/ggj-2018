import { timeline } from "popmotion";

// Classes
import BaseState from "./BaseState";

// Constants
import { BGM_THEME } from "../constants/audio";
import { LOGO_STATE } from "../constants/states";
import { SPLASH_IMAGE } from "../constants/sprites";

export default class SplashState extends BaseState {
  create () {
    this.sound.play(BGM_THEME);

    const sprite = this.add.sprite(427, 240, SPLASH_IMAGE);
    sprite.alpha = 0;
    sprite.anchor.set(.5);

    timeline([
      { track: "fadeIn", from: 0, to: 1, duration: 500 },
      1500,
      { track: "fadeOut", from: 0, to: 1, duration: 500 }
    ]).start({
      complete: () => {
        this.game.state.start(LOGO_STATE);
      },
      update: (data) => {
        sprite.alpha = data.fadeIn - data.fadeOut;
      }
    });
  }
}
