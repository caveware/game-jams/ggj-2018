import BaseState from "./BaseState";
import BouncingIcon from "../classes/BouncingIcon";
import Calendar from "../classes/Calendar";
import Email from "../classes/Email";
import Level from "../classes/Level";
import Notes from "../classes/Notes";
import TextBox from "../classes/TextBox";
import Ticker from "../classes/Ticker";

import { easing, timeline, tween } from "popmotion";

import { GAME_WIDTH, GAME_HEIGHT } from "../constants/display";
import LEVELS from "../levels/index";
import {
  BGM_1,
  BGM_2,
  SFX_CLICK,
  SFX_CORRECT,
  SFX_DROP,
  SFX_GRAB,
  SFX_KEYPRESS_1,
  SFX_KEYPRESS_2,
  SFX_KEYPRESS_3,
  SFX_WHOOSH,
  SFX_WRONG
} from "../constants/audio";
import {
  CLUE_NUMBER,
  CLUE_WORD,
  DESK_BUTTON_CONTINUE,
  DESK_BUTTON_EMAIL,
  DESK_BUTTON_NOTES,
  DESK_CORKBOARD,
  DESK_GOVERNMENT,
  DESK_REBELLION,
  DESK_RESULT_CORRECT,
  DESK_RESULT_WRONG,
  DESK_TICKER_PAUSE,
  DESK_TICKER_PLAY,
  FADE
} from "../constants/sprites";
import { CLUE_TEXT } from "../constants/styles";

const BGM = [
  BGM_1,
  BGM_2
];

const KEYPRESSES = [
  SFX_KEYPRESS_1,
  SFX_KEYPRESS_2,
  SFX_KEYPRESS_3
];

export default class DeskState extends BaseState {
  // Interface
  background: Phaser.Sprite;
  bgm: Phaser.Sound;
  calendar: Calendar;
  clues: Phaser.Group;
  completed: boolean;
  continue: Phaser.Sprite;
  correctIcon: BouncingIcon;
  email: Email;
  emailButton: Phaser.Sprite;
  fade: Phaser.Sprite;
  forceTickerPause: boolean;
  inputMessage: string;
  level: Level;
  levelJustStarted: boolean;
  levelNumber: number;
  lockActivity: boolean;
  notes: Notes;
  notesButton: Phaser.Sprite;
  outputMessage: string;
  step: number;
  strongLockActivity: boolean;
  textbox: TextBox;
  ticker: Ticker;
  tickerControl: Phaser.Sprite;
  wrongIcon: BouncingIcon;

  init (level: number) {
    this.level = new Level(LEVELS[level]);
    this.levelNumber = level;

    localStorage.setItem("CURRENT_LEVEL", "" + this.levelNumber);
  }

  create () {
    // Start music
    this.bgm = this.sound.play(BGM[this.levelNumber % BGM.length], .25, true);

    // Whether or not to lock clue movement and text input
    this.lockActivity = false;
    this.completed = false;

    // Create background
    this.background = this.add.sprite(0, 0, this.level.config.corkboard || DESK_CORKBOARD);

    // Create calendar
    this.calendar = new Calendar(this.game, this.levelNumber);
    this.calendar.position.set(40, 10);

    // Add clues
    this.clues = this.add.group();
    const bounds = new Phaser.Rectangle(60, 60, GAME_WIDTH - 120, GAME_HEIGHT - 120);
    this.level.config.clues.forEach((clueId) => {
      this.createClue(clueId, bounds);
    });
    Object.keys(this.level.cipherList).forEach((listId) => {
      const list = this.level.cipherList[listId].ciphers;

      list.forEach((cipher) => {
        if (cipher.hideClue) return;

        if (cipher.keyword) {
          this.createClue(CLUE_WORD, bounds, cipher.showLength
            ? `[${cipher.keyword.length} LETTERS]`
            : cipher.keyword);
        } else if (cipher.keycode) {
          this.createClue(CLUE_NUMBER, bounds, cipher.keycode);
        }
      });
    });

    // Hackingly interpret first input
    this.step = 0;
    this.inputMessage = this.level.inputs[0];
    this.outputMessage = this.level.outputs[0];
    let sender = this.level.senders[0];

    if (!this.inputMessage) {
      this.inputMessage = this.outputMessage = "NO_MESSAGES_TODAY";
      this.completed = true;
      sender = "SYSTEM";
    }

    // Create and position textbox
    this.textbox = new TextBox(this.game, this.inputMessage.length);
    this.textbox.position.set(GAME_WIDTH / 2, GAME_HEIGHT - 30);

    // Create and position ticker
    this.ticker = new Ticker(this.game, sender, this.inputMessage, this.outputMessage, {
      onComplete: () => {
        if (this.completed) {
          this.continue.visible = true;
          this.fade.alpha = .5;
        }
      },
      stopText: true
    });
    this.ticker.position.set(GAME_WIDTH / 2 + 100, 30);
    this.forceTickerPause = false;

    // Create ticker control
    this.tickerControl = this.add.sprite(205, 5, DESK_TICKER_PAUSE);
    this.tickerControl.inputEnabled = true;
    this.tickerControl.input.useHandCursor = true;
    this.tickerControl.events.onInputDown.add(() => {
      this.sound.play(SFX_CLICK);
      this.forceTickerPause = !this.forceTickerPause;
      this.tickerControl.loadTexture(this.forceTickerPause
        ? DESK_TICKER_PLAY
        : DESK_TICKER_PAUSE);
    });

    // Create and position buttons
    this.emailButton = this.add.sprite(72, 448, DESK_BUTTON_EMAIL);
    this.emailButton.inputEnabled = true;
    this.emailButton.input.useHandCursor = true;
    this.emailButton.events.onInputDown.add(() => {
      this.sound.play(SFX_CLICK);
      if (this.email.visible) {
        this.closeEmail();
      } else {
        this.openEmail();
      }
    });
    this.emailButton.anchor.set(.5);
    this.notesButton = this.add.sprite(GAME_WIDTH - 72, 448, DESK_BUTTON_NOTES);
    this.notesButton.inputEnabled = true;
    this.notesButton.input.useHandCursor = true;
    this.notesButton.events.onInputDown.add(() => {
      this.sound.play(SFX_CLICK);
      if (this.notes.visible) {
        this.closeNotes();
      } else {
        this.openNotes();
      }
    });
    this.notesButton.anchor.set(.5);

    // Create and position email
    const email = this.level.config.email;
    this.email = new Email(this, GAME_WIDTH / 2, GAME_HEIGHT, {
      close: this.closeEmail.bind(this)
    }, email);

    // Create and position notes
    this.notes = new Notes(this, GAME_WIDTH / 2, GAME_HEIGHT, {
      close: this.closeNotes.bind(this)
    });

    // Create icons
    this.correctIcon = new BouncingIcon(this.game, GAME_WIDTH / 2, GAME_HEIGHT / 2, DESK_RESULT_CORRECT);
    this.wrongIcon = new BouncingIcon(this.game, GAME_WIDTH / 2, GAME_HEIGHT / 2, DESK_RESULT_WRONG);

    this.strongLockActivity = true;
    timeline([
      { track: "fade", from: 1, to: 0, duration: 1000 },
      500
    ]).start({
      complete: () => {
        this.fade.alpha = 0;
        this.levelJustStarted = true;
        this.openEmail(true);
      },
      update: (data) => {
        this.fade.alpha = data.fade;
      }
    });

    this.fade = new Phaser.Sprite(this.game, 0, 0, FADE);
    this.game.stage.addChild(this.fade);

    // Create and position continue button
    this.continue = new Phaser.Sprite(this.game, GAME_WIDTH / 2, GAME_HEIGHT / 2, DESK_BUTTON_CONTINUE);
    this.continue.anchor.set(.5);
    this.continue.inputEnabled = true;
    this.continue.input.useHandCursor = true;
    this.continue.events.onInputDown.add(() => {
      this.sound.play(SFX_CORRECT);
      this.progress();
    });
    this.continue.visible = false;
    this.game.stage.addChild(this.continue);
  }

  addLetter (key: string) {
    const textbox = this.getTypeBox();
    if (!textbox || this.strongLockActivity) return;
    if (textbox.addLetter(key)) this.playKeypressSound();
  }

  closeEmail () {
    if (this.levelNumber === 30) {
      this.fade.alpha = .5;

      const government = new Phaser.Sprite(this.game, 227, 240, DESK_GOVERNMENT);
      government.anchor.set(.5);
      government.inputEnabled = true;
      government.input.useHandCursor = true;
      this.stage.addChild(government);

      const rebellion = new Phaser.Sprite(this.game, 627, 240, DESK_REBELLION);
      rebellion.anchor.set(.5);
      rebellion.inputEnabled = true;
      rebellion.input.useHandCursor = true;
      this.stage.addChild(rebellion);

      const goTo = (value: number) => {
        government.destroy();
        rebellion.destroy();

        this.goToNextLevel(value);
      };

      government.events.onInputDown.add(() => {
        goTo(31);
      });
      rebellion.events.onInputDown.add(() => {
        goTo(39);
      });
    } else if (this.levelJustStarted) {
      this.levelJustStarted = false;

      if (this.level.config.messages.length) {
        this.strongLockActivity = false;
      }

      timeline([
        { track: "meme1", from: 0, to: 0, duration: 0 },
        1000 + Math.random() * 4000,
        { track: "meme2", from: 0, to: 0, duration: 0 }
      ]).start({
        complete: () => {
          this.ticker.start();
        }
      });
    }

    if (this.email.visible) this.sound.play(SFX_WHOOSH);

    this.lockActivity = false;
    this.email.hide();
  }

  closeNotes () {
    if (this.notes.visible) this.sound.play(SFX_WHOOSH);

    this.lockActivity = false;
    this.notes.hide();
  }

  createClue (sprite: string, bounds: Phaser.Rectangle, text?: string) {
    const x = 200 + (GAME_WIDTH - 400) * Math.random();
    const y = 200 + (GAME_HEIGHT - 400) * Math.random();
    const clue = this.add.sprite(x, y, sprite);
    clue.anchor.set(.5);

    // Set up input
    clue.inputEnabled = true;
    clue.input.enableDrag(true);
    clue.input.boundsRect = bounds;
    clue.input.useHandCursor = true;

    // Set events
    clue.events.onInputDown.add(() => {
      this.sound.play(SFX_GRAB);
      clue.rotation = -.025;
    });
    clue.events.onInputUp.add(() => {
      this.sound.play(SFX_DROP);
      clue.rotation = 0;
    });

    // Create text if need to
    if (text) {
      const textComponent = new Phaser.Text(this.game, 0, 10, text.toUpperCase(), CLUE_TEXT);
      textComponent.anchor.set(.5);
      clue.addChild(textComponent);
    }

    this.clues.add(clue);
  }

  getTypeBox () {
    if (this.notes.visible) {
      return this.notes;
    } else if (!this.email.visible) {
      return this.textbox;
    }

    return null;
  }

  goToNextLevel (level?: number) {
    level = level || this.levelNumber + 1;

    if (this.level.config.nextState) {
      this.game.state.start(this.level.config.nextState);
    } else {
      tween({
        duration: 250,
        from: 0,
        to: 1
      }).start({
        complete: () => {
          this.fade.alpha = 0;
          this.game.state.restart(true, false, level);
        },
        update: (value) => {
          this.fade.alpha = value;
        }
      });
    }
  }

  isLocked () {
    return this.lockActivity || this.strongLockActivity;
  }

  openEmail (force: boolean = false) {
    if (this.strongLockActivity && !force) return false;

    this.closeNotes();
    this.lockActivity = true;
    this.email.show();
  }

  openNotes () {
    if (this.strongLockActivity) return false;

    this.closeEmail();
    this.lockActivity = true;
    this.notes.show();
  }

  playKeypressSound () {
    const key = KEYPRESSES[Math.floor(Math.random() * KEYPRESSES.length)];
    this.sound.play(key);
  }

  pressEnter () {
    const textbox = this.getTypeBox();
    if (this.strongLockActivity) return;
    const text = (textbox as TextBox).pressEnter();
    if (text && textbox === this.textbox) {
      this.playKeypressSound();
      if (text === this.outputMessage) {
        this.sound.play(SFX_CORRECT, .5);
        this.correctIcon.show();
        this.forceTickerPause = false;
        this.tickerControl.loadTexture(DESK_TICKER_PAUSE);
        this.completed = true;
        this.strongLockActivity = true;
        this.ticker.setText(this.level.config.messages[this.step].full.trim());
      } else {
        this.sound.play(SFX_WRONG, .5);
        this.wrongIcon.show();
        this.notes.texts = ["", text, ...this.notes.texts.slice(1, 8)];
        this.notes.updateTexts();

        const startY = this.notesButton.position.y;
        tween({
          duration: 250,
          ease: easing.backOut,
          from: 0,
          to: 1
        }).start({
          update: (value) => {
            this.notesButton.position.y = startY - Math.sin(value * Math.PI) * 80;
          }
        });
      }
    }
  }

  progress () {
    this.continue.visible = false;
    this.fade.alpha = 0;
    this.step++;

    const nextMessage = this.level.config.messages[this.step];

    if (nextMessage) {
      this.completed = false;
      this.strongLockActivity = false;

      this.inputMessage = this.level.inputs[this.step];
      this.outputMessage = this.level.outputs[this.step];
      const sender = this.level.senders[this.step];

      this.textbox.setMaxLength(this.inputMessage.length);
      this.ticker.setStyle(sender);
      this.ticker.setText(this.inputMessage, this.outputMessage);
    } else {
      this.goToNextLevel();
    }
  }

  removeLetter () {
    const textbox = this.getTypeBox();
    if (!textbox || this.strongLockActivity) return;
    if (textbox.removeLetter()) this.playKeypressSound();
  }

  shutdown () {
    this.bgm.stop();
    this.calendar.destroy();
    this.correctIcon.destroy();
    this.email.destroy();
    this.fade.destroy();
    this.notes.destroy();
    this.textbox.destroy();
    this.ticker.destroy();
    this.wrongIcon.destroy();
  }

  update () {
    const dt = this.getDeltaTime(this.game);
    if (!this.levelJustStarted && !this.forceTickerPause) this.ticker.updateTicker(dt);

    this.clues.ignoreChildInput = this.isLocked();
    if (this.clues.ignoreChildInput) return;

    this.clues.children.forEach((clue) => {
      if ((clue as Phaser.Sprite).input.isDragged) {
        this.clues.bringToTop(clue);
      }
    });
  }
}