import KeyConfig from "../interfaces/KeyConfig";

export default class NoKey implements KeyConfig {

  keyType = "none";

  constructor () { }

  generateKey() { }

}