import { ALPHABET } from "../constants/ciphers";
import Cipher from "../interfaces/Cipher";
import NumericKey from "../keyConfigs/NumericKey";
import { normaliseString } from "../helpers/cipher";

export default class NumericCipher implements Cipher {
  keycode: number;
  hideClue: true;

  constructor(public keyConf: NumericKey = new NumericKey(0, 0)) {
    this.keycode = keyConf.generateKey();
  }

  decode(text: string) {
    let texti = text;
    const pairs = [];
    while (text.length) {
      pairs.push(text.slice(0, 2));
      text = text.slice(2);
    }
    return pairs.map((char) => char === 26 + this.keycode
      ? "_"
      : String.fromCharCode(char - this.keycode)
    ).join("");
  }

  encode(text: string) {
    return normaliseString(text).split("").map(
      (char) => {
        let num;
        let charC = char.charCodeAt(0) - "A".charCodeAt(0);
        if (charC < 0 || charC > 25) num = 26;
        else num = charC;
        num +=  this.keycode;
        let str = num.toString();
        if (num < 10) {
          str = "0" + str;
        }
        return str;
      }).join("");
  }
}