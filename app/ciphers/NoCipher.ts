import Cipher from "../interfaces/Cipher";
import NoKey from "../keyConfigs/NoKey";
import { normaliseString } from "../helpers/cipher";

export default class NoCipher implements Cipher {

  constructor(keyConf: NoKey = new NoKey()) { }

  decode (text: string) {
    return normaliseString(text);
  }

  encode (text: string) {
    return normaliseString(text);
  }
}