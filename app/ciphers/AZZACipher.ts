import SubstitutionCipher from "./SubstitutionCipher";
import { ALPHABET } from "../constants/ciphers";
import StaticStringKey from "../keyConfigs/StaticStringKey";

export default class AZZACipher extends SubstitutionCipher {
  constructor () {
    super(new StaticStringKey(ALPHABET.split("").reverse().join("")));
  }
}