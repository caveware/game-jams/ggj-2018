import Cipher from "../interfaces/Cipher";
import { ALPHABET, ALPHABET_WITH_UNDERSCORE } from "../constants/ciphers";
import StringKey from "../keyConfigs/StringKey";
import { normaliseString } from "../helpers/cipher";

export default class ColumnCipher implements Cipher {
    private keyword: string;
    hideClue = true;

  constructor (private keyConf: StringKey = new StringKey(2, 5)) {
      this.keyword = keyConf.generateKey();
   }

  encode (text: string) {
    let keyword = this.keyword;

    let paddedText = normaliseString(text);
    if (text.length % keyword.length !== 0) {
        for (let i = 0; i < keyword.length - (text.length % keyword.length); i++) {
            paddedText += "_";
        }
    }
    let colLength = paddedText.length / keyword.length;

    const chars = ALPHABET_WITH_UNDERSCORE;

    let ciphertext = "";
    let k = 0;
    for (let i = 0; i < keyword.length; i++) {
      let t;
        while (k < ALPHABET_WITH_UNDERSCORE.length) {
            t = keyword.indexOf(chars.charAt(k));
            let arrkw = keyword.split(""); arrkw[t] = "_"; keyword = arrkw.join("");
            if (t >= 0) break;
            else k++;
        }
        for (let j = 0; j < colLength; j++) ciphertext += paddedText.charAt(j * keyword.length + t);
    }
    return ciphertext;
  }

  decode (text: string) {
    let ciphertext = normaliseString(text);
    let keyword = this.keyword;
    let klen = keyword.length;

    // first we put the text into columns based on keyword length
    let cols = new Array(klen);
    let colLength = ciphertext.length / klen;
    for (let i = 0; i < klen; i++) cols[i] = ciphertext.substr(i * colLength, colLength);

    // now we rearrange the columns so that they are in their unscrambled state
    let newcols = new Array(klen);
    const chars = ALPHABET_WITH_UNDERSCORE;
    let j = 0;
    let i = 0;
    while (j < klen) {
        let t = keyword.indexOf(chars.charAt(i));
        if (t >= 0) {
            newcols[t] = cols[j++];
            let arrkw = keyword.split(""); arrkw[t] = "_"; keyword = arrkw.join("");
        } else i++;
    }
    // now read off the columns row-wise
    let plaintext = "";
    for (let i = 0; i < colLength; i++) {
        for (let j = 0; j < klen; j++) plaintext += newcols[j].charAt(i);
    }
    return plaintext;
}

}