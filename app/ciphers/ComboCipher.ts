import Cipher from "../interfaces/Cipher";
import KeyConfig from "../interfaces/KeyConfig";
import { normaliseString } from "../helpers/cipher";

export default class ComboCipher implements Cipher {
  ciphers: Cipher[];

  hideClue = false;

  constructor (ciphers: Cipher[]) {
    this.ciphers = ciphers.map((CipherClass: any) => {
      if (!Array.isArray(CipherClass)) {
        let c = new CipherClass();
        c.hideClue = true;
        return c;
      }
      else {
        let c = new CipherClass[0](CipherClass[1]);
        c.hideClue = CipherClass.length < 3 || !CipherClass[2];
        return c;
      }
    });
  }

  decode (text: string) {
    const reversedCiphers = [...this.ciphers].reverse();

    return reversedCiphers.reduce((text, cipher) => {
      return cipher.decode(text);
    }, text);
  }

  encode (text: string) {
    text = normaliseString(text);
    return this.ciphers.reduce((text, cipher) => {
      return cipher.encode(text);
    }, text);
  }
}