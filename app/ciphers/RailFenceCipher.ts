import Cipher from "../interfaces/Cipher";
import KeyConfig from "../interfaces/KeyConfig";
import NumericKey from "../keyConfigs/NumericKey";
import { normaliseString } from "../helpers/cipher";

declare function decodeRailFenceCipher(a: any, b: any);
declare function encodeRailFenceCipher(a: any, b: any);

export default class RailFenceCipher implements Cipher {
  private width: number;

  constructor (public keyConf: NumericKey = new NumericKey(2, 5)) {
    this.width = keyConf.generateKey();
   }

  decode (text: string) {
    return decodeRailFenceCipher(normaliseString(text), this.width);
  }

  encode (text: string) {
    return encodeRailFenceCipher(normaliseString(text), this.width);
  }
}