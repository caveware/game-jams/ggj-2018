import { DESK_PAPER } from "../constants/sprites";
import { GENERAL_INPUT } from "../constants/styles";

export default class TextBox extends Phaser.Sprite {
  text: Phaser.Text;

  constructor (scene: Phaser.Game, public maximumLength: number = 12, text: string = "") {
    super(scene, 0, 0, DESK_PAPER);
    this.anchor.set(.5);
    scene.stage.addChild(this);

    this.text = new Phaser.Text(scene, 0, 3, text, GENERAL_INPUT);
    this.text.anchor.set(.5);
    this.addChild(this.text);
  }

  addLetter (character: string) {
    if (this.text.text.length < this.maximumLength) {
      this.text.text += character;
      return true;
    }
  }

  pressEnter () {
    if (this.text.text.length) {
      const data = this.text.text;
      this.text.text = "";
      return data;
    }
  }

  removeLetter () {
    if (this.text.text.length > 0) {
      this.text.text = this.text.text.slice(0, -1);
      return true;
    }
  }

  setMaxLength (max: number) {
    this.text.text = "";
    this.maximumLength = max;
  }
}
