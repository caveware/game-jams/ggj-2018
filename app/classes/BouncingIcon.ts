import { easing, timeline, tween } from "popmotion";
const { anticipate, createAnticipateEasing, createReversedEasing } = easing;

export default class BouncingIcon extends Phaser.Sprite {
  constructor (stage: Phaser.Game, x: number, y: number, icon: string) {
    super(stage, x, y, icon);
    this.anchor.set(.5);
    this.scale.set(0);
    this.visible = false;
    stage.stage.addChild(this);
  }

  show () {
    const anti = createAnticipateEasing(5);
    const reversed = createReversedEasing(anti);
    this.visible = true;

    timeline([
      { track: "zoomIn", from: 0, to: 1, duration: 250, ease: reversed },
      1000,
      { track: "zoomOut", from: 0, to: 1, duration: 250, ease: anti }
    ]).start({
      complete: () => {
        this.visible = false;
      },
      update: (data) => {
        this.scale.set(data.zoomIn - data.zoomOut);
      }
    });
  }
}