import Cipher from "../interfaces/Cipher";
import ComboCipher from "../ciphers/ComboCipher";
import LevelConfig from "../interfaces/LevelConfig";

import { normaliseString } from "../helpers/cipher";

import { parseTemplate, parseTemplateWithoutTags } from "../helpers/templates";

export default class Level {
  public cipherList: object;
  public inputs: string[];
  public outputs: string[];
  public senders: string[];

  constructor (public config: LevelConfig) {
    // Create internal cipher list
    this.cipherList = {};
    Object.keys(config.cipherList).forEach((key) => {
      const cipherSet = config.cipherList[key];
      const list = cipherSet[Math.floor(Math.random() * cipherSet.length)];
      this.cipherList[key] = new ComboCipher(list);
    });

    // Determine inputs and outputs
    this.inputs = [];
    this.outputs = [];
    this.senders = [];
    config.messages.forEach((message) => {
      this.inputs.push(normaliseString(parseTemplate(message.text, this.cipherList)));
      if (config.outputCipher) {
        this.outputs.push(
          (config.outputCipher as Cipher).encode(
            normaliseString(
              parseTemplateWithoutTags(message.text, this.cipherList)
            )
          )
        );
      }
      else {
        this.outputs.push(
          normaliseString(
            parseTemplateWithoutTags(message.text, this.cipherList)
          )
        );
      }
      this.senders.push(message.sender);
    });
  }
}