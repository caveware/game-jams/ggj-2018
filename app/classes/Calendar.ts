import { DAYS_IN_MONTH, FIRST_DAY, MONTHS } from "../constants/date";
import { CALENDAR_STYLE } from "../constants/styles";

export default class Calendar extends Phaser.Text {
  constructor (state: Phaser.Game, day: number) {
    super(state, 0, 0, "", CALENDAR_STYLE);
    this.text = this.determineText(day);
    state.stage.addChild(this);
  }

  determineText (day: number) {
    const id = FIRST_DAY + day;
    const MONTH = MONTHS[Math.floor(id / DAYS_IN_MONTH)] || "NO_MONTH";
    const DAY = (id % DAYS_IN_MONTH) + 1;
    const suffixes = [ "ST", "ND", "RD" ];

    return `${DAY}${suffixes[id + 1] || "TH"} ${MONTH}`;
  }
}