import { Range } from "../helpers/range";

import data from "../../assets/words/en_AU";

export default class Dictionary {
  private map;

  constructor() { this.map = data; }

  getWordOfLen(strLen: number) {
    if (typeof this.map[strLen.toString()] === "undefined")
    throw new Error("Dictionary does not contain any words of length " + strLen);
    let wordIdx = Math.floor(Math.random() * this.map[strLen].length);
    return this.map[strLen][wordIdx];
  }

  getWordInLenRange(minLen: number, maxLen: number) {
    let range = Range(minLen, maxLen + 1);
    while (range.length > 0) {
      let strIdx = Math.floor(Math.random() * range.length).toString();
      try {
        return this.getWordOfLen(range[strIdx]);
      } catch (e) {
        const index = range.indexOf(range[strIdx]);

        if (index !== -1) {
            range.splice(index, 1);
        }
      }
    }
    throw new Error("No words found in range " + minLen + ":" + maxLen);
  }
}