import { tween } from "popmotion";

import { DESK_NOTES, DESK_EMAIL_CLOSE } from "../constants/sprites";
import { NOTE_TEXT } from "../constants/styles";
import DeskState from "../states/DeskState";

interface Callbacks {
  close: Function;
}

export default class Notes extends Phaser.Sprite {
  close: Phaser.Sprite;
  defaultX: number;
  textComponents: Phaser.Text[];
  texts: string[];
  tween?: any;

  constructor (private state: Phaser.State, x: number, y: number, callbacks: Callbacks) {
    super(state.game, x, y, DESK_NOTES);
    this.anchor.set(.5, 1);
    state.game.stage.addChild(this);

    // Create texts
    this.texts = [""];
    this.textComponents = [];
    for (let i = 0; i < 9; i++) {
      const text = new Phaser.Text(state.game, 0, -335 + i * 40, "", NOTE_TEXT);
      text.anchor.set(.5);
      this.addChild(text);
      this.textComponents.push(text);
    }

    // Hide by default
    this.alpha = 0;
    this.defaultX = x;
    this.visible = false;

    // Create close button
    this.close = new Phaser.Sprite(state.game, 212, -394, DESK_EMAIL_CLOSE);
    this.close.inputEnabled = true;
    this.close.input.useHandCursor = true;
    this.close.events.onInputDown.add(callbacks.close, state);
    this.addChild(this.close);
  }

  addLetter (key: string) {
    const text = this.texts[0];
    if (text.length < 16) {
      this.texts[0] += key;
      this.updateTexts();
      return true;
    }
  }

  hide () {
    if (!this.visible) return;

    if (this.tween) this.tween.stop();

    this.tween = tween({
      duration: 250,
      from: this.scale.x,
      to: .5,
    }).start({
      complete: () => {
        this.visible = false;
      },
      update: this.updateTween.bind(this)
    });
  }

  pressEnter () {
    if (this.texts[0].length > 0) {
      this.texts = ["", ...this.texts.slice(0, 8)];
      this.updateTexts();
      return true;
    }
  }

  removeLetter () {
    const text = this.texts[0];
    if (text.length) {
      this.texts[0] = text.slice(0, -1);
      this.updateTexts();
      return true;
    }
  }

  show () {
    if (this.visible) return;

    this.visible = true;
    this.alpha = 0;
    this.scale.set(.5);

    if (this.tween) this.tween.stop();

    this.tween = tween({
      duration: 250,
      from: .5,
      to: 1,
    }).start({
      update: this.updateTween.bind(this)
    });
  }

  updateTween (value) {
    this.alpha = value * 2 - 1;
    this.scale.set(value * value);
    this.position.x = this.defaultX + (1 - this.alpha) * 300;
  }

  updateTexts () {
    this.textComponents.forEach((component, key) => {
      component.text = this.texts[key] || "";
    });
  }
}