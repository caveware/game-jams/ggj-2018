import { tween } from "popmotion";

import { DESK_EMAIL, DESK_EMAIL_ARROW, DESK_EMAIL_CLOSE } from "../constants/sprites";
import {
  EMAIL_NAME_STYLE,
  EMAIL_ROLE_STYLE,
  EMAIL_TEXT_STYLE,
  EMAIL_TITLE_STYLE
} from "../constants/styles";
import DeskState from "../states/DeskState";

interface Callbacks {
  close: Function;
}

interface EmailData {
  content: string;
  sender: {
    icon: string;
    name: string;
    role: string;
  };
  title: string;
}

export default class Email extends Phaser.Sprite {
  close: Phaser.Sprite;
  content: Phaser.Text;
  defaultX: number;
  icon?: Phaser.Sprite;
  scrollDown?: Phaser.Sprite;
  scrollUp?: Phaser.Sprite;
  tween?: any;

  constructor (private state: Phaser.State, x: number, y: number, callbacks: Callbacks, private email?: EmailData) {
    super(state.game, x, y, DESK_EMAIL);

    this.anchor.set(.5, 1);
    state.game.stage.addChild(this);

    // Hide by default
    this.alpha = 0;
    this.defaultX = x;
    this.visible = false;

    // Create close button
    this.close = new Phaser.Sprite(state.game, 112, -394, DESK_EMAIL_CLOSE);
    this.close.inputEnabled = true;
    this.close.input.useHandCursor = true;
    this.close.events.onInputDown.add(callbacks.close, state);
    this.addChild(this.close);

    if (email) {
      // Create sender profile
      this.icon = new Phaser.Sprite(state.game, -85, -300, email.sender.icon);
      this.icon.anchor.set(.5);
      this.addChild(this.icon);
      const name = new Phaser.Text(state.game, -40, -315, email.sender.name, EMAIL_NAME_STYLE);
      name.anchor.set(0, .5);
      this.addChild(name);
      const role = new Phaser.Text(state.game, -40, -285, email.sender.role, EMAIL_ROLE_STYLE);
      role.anchor.set(0, .5);
      this.addChild(role);
      const title = new Phaser.Text(state.game, -140, -240, email.title, EMAIL_TITLE_STYLE);
      title.anchor.set(0, .5);
      this.addChild(title);

      const contentMask = new Phaser.Graphics(this.game, -140, -220);
      contentMask.beginFill(0xffffff);
      contentMask.drawRect(0, 0, 280, 210);
      this.addChild(contentMask);

      this.content = new Phaser.Text(state.game, 0, -220, email.content.trim(), EMAIL_TEXT_STYLE);
      this.content.anchor.set(.5, 0);
      this.content.mask = contentMask;
      this.content.inputEnabled = true;
      this.content.wordWrap = true;
      this.content.wordWrapWidth = 280;
      this.addChild(this.content);

      this.scrollDown = this.game.add.sprite(140, -10, DESK_EMAIL_ARROW);
      this.scrollDown.anchor.set(.5);
      this.scrollDown.rotation = Math.PI;
      this.addChild(this.scrollDown);
      this.scrollUp = this.game.add.sprite(140, -225, DESK_EMAIL_ARROW);
      this.scrollUp.anchor.set(.5);
      this.addChild(this.scrollUp);

      this.updateScrollButtons();
    } else {
      this.content = new Phaser.Text(state.game, 0, -220, "No new emails today", EMAIL_ROLE_STYLE);
      this.content.anchor.set(.5, .5);
      this.addChild(this.content);
    }
  }

  hide () {
    if (!this.visible) return;

    if (this.tween) this.tween.stop();

    this.tween = tween({
      duration: 250,
      from: this.scale.x,
      to: .5,
    }).start({
      complete: () => {
        this.visible = false;
      },
      update: this.updateTween.bind(this)
    });

    this.game.input.mouse.mouseDownCallback = null;
    this.game.input.mouse.mouseUpCallback = null;
  }

  show () {
    if (this.visible) return;

    this.visible = true;
    this.alpha = 0;
    this.scale.set(.5);

    if (this.tween) this.tween.stop();

    this.tween = tween({
      duration: 250,
      from: .5,
      to: 1,
    }).start({
      update: this.updateTween.bind(this)
    });

    // Add scroll events
    this.game.input.mouse.mouseWheelCallback = () => {
      const newPos = (20 * this.game.input.mouse.wheelDelta);
      this.content.position.y += 220;
      this.content.position.y = Math.max(this.content.position.y + newPos, -this.content.height + 210);
      this.content.position.y = Math.min(0, this.content.position.y);
      this.content.position.y -= 220;

      this.updateScrollButtons();
    };
  }

  updateScrollButtons () {
    const y = this.content.position.y + 220;
    this.scrollUp.alpha = y === 0 ? .3 : 1;
    this.scrollDown.alpha = y === (-this.content.height + 210) ? .3 : 1;
  }

  updateTween (value) {
    this.alpha = value * 2 - 1;
    this.scale.set(value * value);
    this.position.x = this.defaultX - (1 - this.alpha) * 300;
  }
}