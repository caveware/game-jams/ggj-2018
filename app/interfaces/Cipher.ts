export default interface Cipher {
  decode: Function;
  encode: Function;
  hideClue?: boolean;
}