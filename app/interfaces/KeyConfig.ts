export default interface KeyConfig {
  keyType: string;

  generateKey: Function;
}