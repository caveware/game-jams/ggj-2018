interface Message {
  full: string;
  sender: string;
  text: string;
}

export default interface LevelConfig {
  cipherList: object;
  outputCipher?: object;
  clues: string[];
  corkboard?: string;
  email?: {
    content: string;
    sender: {
      icon: string;
      name: string;
      role: string;
    };
    title: string;
  };
  messages: Message[];
  nextState?: string;
}