import {
  AVATAR_DAVIDSON,
  AVATAR_GLYNNE,
  AVATAR_BURNELL,
  AVATAR_MCLURE,
  AVATAR_SILVERSTONE,
  AVATAR_THOMPSON,
  AVATAR_CHARLES,
} from "../constants/sprites";

interface Person {
  icon: string;
  name: string;
  role: string;
}

export const DAVIDSON: Person = {
  icon: AVATAR_DAVIDSON,
  name: "Carmel Davidson",
  role: "Rebel Leader"
};

export const GLYNNE: Person = {
  icon: AVATAR_GLYNNE,
  name: "Lindsay Glynne",
  role: "Rebel Decipherer"
};

export const BURNELL: Person = {
  icon: AVATAR_BURNELL,
  name: "Kate Burnell",
  role: "ARD Spy"
};

export const MCLURE: Person = {
  icon: AVATAR_MCLURE,
  name: "James McLure",
  role: "ARD Commander"
};

export const SILVERSTONE: Person = {
  icon: AVATAR_SILVERSTONE,
  name: "Tom Silverstone",
  role: "Rebel Leader"
};

export const THOMPSON: Person = {
  icon: AVATAR_THOMPSON,
  name: "Gary Thompson",
  role: "ARD Commander"
};

export const CHARLES: Person = {
  icon: AVATAR_CHARLES,
  name: "Sebastian Charles",
  role: "Ex-ARD Agent"
};