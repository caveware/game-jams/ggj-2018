export const CREDITS_STATE = "CREDITS_STATE";
export const DESK_STATE = "DESK_STATE";
export const MAIN_STATE = "MAIN_STATE";
export const LOGO_STATE = "LOGO_STATE";
export const PRELOAD_STATE = "PRELOAD_STATE";
export const SAVE_STATE = "SAVE_STATE";
export const SPLASH_STATE = "SPLASH_STATE";
