export const A = 65;
export const BACKSPACE = 8;
export const ENTER = 13;
export const SPACE = 32;
export const UNDERSCORE_HYPHEN = 189;
export const Z = 90;
