export const BGM_1 = "BGM_1";
export const BGM_2 = "BGM_2";
export const BGM_CREDITS = "BGM_CREDITS";
export const BGM_THEME = "BGM_THEME";

export const BGM_LIST = {
  [BGM_1]: "bg1.mp3",
  [BGM_2]: "bg2.mp3",
  [BGM_CREDITS]: "credits.mp3",
  [BGM_THEME]: "theme.mp3"
};
export const BGM_PATH = "bgm/";


export const SFX_CLICK = "SFX_CLICK";
export const SFX_CORRECT = "SFX_CORRECT";
export const SFX_DROP = "SFX_DROP";
export const SFX_GRAB = "SFX_GRAB";
export const SFX_KEYPRESS_1 = "SFX_KEYPRESS_1";
export const SFX_KEYPRESS_2 = "SFX_KEYPRESS_2";
export const SFX_KEYPRESS_3 = "SFX_KEYPRESS_3";
export const SFX_WHOOSH = "SFX_WHOOSH";
export const SFX_WRONG = "SFX_WRONG";

export const SFX_LIST = {
  [SFX_CLICK]: "click.mp3",
  [SFX_CORRECT]: "desk/correct.mp3",
  [SFX_DROP]: "desk/drop.mp3",
  [SFX_GRAB]: "desk/grab.mp3",
  [SFX_KEYPRESS_1]: "desk/keypress_1.mp3",
  [SFX_KEYPRESS_2]: "desk/keypress_2.mp3",
  [SFX_KEYPRESS_3]: "desk/keypress_3.mp3",
  [SFX_WHOOSH]: "desk/whoosh.mp3",
  [SFX_WRONG]: "desk/wrong.mp3"
};
export const SFX_PATH = "sfx/";
