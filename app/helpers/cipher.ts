import * as CODE from "../constants/codes";
import * as KEY from "../constants/keys";

/**
 * Converts a string into an array of character codes (0-25 for letters, 26 for else)
 * @param codes Codes to convert to string
 * @returns {string}
 */
export function convertCodesToString (codes: number[]) {
  return codes.map((code) => {
    if (code === CODE.SPACE) {
      return "_";
    } else {
      return String.fromCharCode(code + KEY.A);
    }
  }).join("");
}

/**
 * Converts a string into an array of character codes (0-25 for letters, 26 for else)
 * @param text Text to convert to codes
 * @returns {number[]}
 */
export function convertStringToCodes (text: string) {
  return text.split("").map((char) => {
    // Determine character code
    const code = char.charCodeAt(0);

    if (code >= KEY.A && code <= KEY.Z) {
      return code - KEY.A;
    } else {
      return CODE.SPACE;
    }
  });
}

export function normaliseString(text: string) {
  return text.replace(/[^\w]/g, "_").toUpperCase();
}