// This will match with our template
const regex = /\{\<([^\>]+)\>([^\{]*?)\}/g;

export function parseTemplate (text: string, cipherList) {
  while (regex.test(text)) {
    text = text.replace(regex, (_, cipherListId, input) => {
      return cipherList[cipherListId].encode(input);
    });
  }

  return text;
}

export function parseTemplateWithoutTags (text: string, cipherList) {
  while (regex.test(text)) {
    text = text.replace(regex, (_, cipherListId, input) => input);
  }

  return text;
}
