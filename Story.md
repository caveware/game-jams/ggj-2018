# Characters
You, the player.

ARD Commander, James McLure

ARD Commander post-McLure, Gary Thompson

ARD Spy, Kate Burnell

Ex-ARD scout, Sebastian Charles

Red team leader, Carmel Davidson

Blue team leader, Tom Silverstone

Red team cipher, Lindsay Glynne

######Voices:
Red: Carmel
Blue: Tom
Green: McLure/Thompson
Pink: Eva
Orange: Jasmin
White: Ceylon(unidentified)

######Scenes:
Govt office
Rebel office

######Sounds:
gunshot


# Opening

You are a new employee in the Utopia Government's Anti-Rebellion Division. Recently, there have been rumours of a new rebellion growing, and you have been put on the team who is tracking them down. As a codebreaker, it is your job to crack the encryption on their messages, so that they can be stopped.

Messages are communication between two rebel groups (United Eastern Uprising [Red] and West Coast Exiles [Blue]) who have as of yet been unable to share any complex encryption keys. They have resorted to sending plain text messages, until they realise someone is watching.

Each day, when you sit at your desk, you get an email from your boss describing the day's work, and any new pins are added to your corkboard. After you have read that, the ticker above your desk starts ticking with the intercepted messages.

### Event 0
Rebels sending messages as plain text. 2 Days

#### Day 1
###### Email - McLure
Welcome to the Anti-Rebellion Division. Ironically enough, Utopia is always under attack by people who don't want the best for society. It is our job to find out their plans and stop them before it's too late.
Any messages we intercept will be played on the ticker in front of you. All you need to do is transbribe them onto the paper in front of you. For technical reasons, spaces have had to be replaced by underscores in all transcription circumstances.
Good luck.
###### Messages
Red: Rumour has it you want to bring down the government. Is this true?
Blue: Why would we want to do anything to harm the institution that has done so much to help society? The part of society who can afford to live there, that is.
Red: We may be able to help you.

#### Day 2
###### Email - McLure
Good job on your first day. We knew there were some groups who were plotting against us, but we hadn't been able to get any evidence of it yet.
It would appear that two rebel groups are attempting to work together. Just keep doing what you are doing, and hopefully we can stop them before anything drastic happens.

###### Messages
Blue: We have been out here for years, trying to find anyone who would be able to help us. Where have you been all this time?
Blue: And how do we know you aren't trying to trick us.
Red: We know you don't have much left. It's now or never isn't it?


### Event 1
Rebels have realised that their messages are being intercepted. Add Casear 1 Cipher. Rebels cipher single important word.

#### Day 3
###### Email - McLure
It would appear that the rebels have realised that we may be listening in to their conversations. They have started enciphering the messages. We have already been able to remove the outer layer of encryption, but some of the words don't seem right. If I had to have a guess, it looks almost like a Casear Cipher. I've added a note to your corkboard with the details.

###### Messages
Blue: They may be {listening}. Even if you aren't them, we have to be careful.
Red: You are {right}. Are you willing to join us?
Blue: This still seems {suspicious}

#### Day 4
###### Email - McLure
Good job deciphering yesterday's messages. Keep up the good work.

###### Messages
Blue: What can you do to {help} us?
Red: I can't {explain} right now, but soon we can show you.
Red: Give us a few {days}, we will have something ready.
Blue: Don't let us {down}

#### Day 5
###### No email
###### Messages
Green: He is doing really {well} so far. No troubles at all.
Green: He may be the {one} we have been looking for.
Green: I don't {know}. Only time will tell...

### Event 2
Rebels have realised that their messages are still being intercepted. Rebels cipher multiple words.

#### Day 6
###### Email - McLure
Good job again on deciphering those messages.
We need to find out if they are going to leave hiding and come out. It seems they are also stepping up their encipherment, ciphering more of the messages. I'm you wont have any trouble with that though.
###### Messages
Red: We have a {package} {ready} to show you our plan. We will need to exchange it in person though.
Blue: {Where} though? We can't risk you drawing the government to our hideout.
Blue: Or {worse}, {drawing} us into a {trap}. We are weak enough already.

#### Day 7
###### Email - McLure
I knew you were the right person. You've had no troubles at all with these messages. Keep it up.
###### Messages
Blue: I may have an {idea}. . We used to have a base of operations to the south of the capital, until we got flushed out. That could work.

### Event 3
Rebels add Azza.

#### Day 8
###### Email - McLure
It seems that they know we are on to them. They have added a new type of cipher to their messages. I've added a new pin to your corkboard.
Good luck
###### Messages
Red: Where is this {place} you {mentioned}? It may be our best shot.
Blue: There is an {abandoned mill} to the {south} of the capital. We can be there in three days.
Red: Be there at {midday sharp}.
Blue: We can't {afford} to be {seen}. Make sure you aren't followed.

#### Day 9
###### Email - McLure
I knew you wouldn't have any trouble with this. We don't know now which cipher they will use, but I am sure you can figure it out.
Good work finding out about their meeting too. This could be a good opportunity to squash any hopes of a resistance before it gets out of hand.
###### Messages
Pink: I'm so excited for Peggy's party next week. It's been too long since I've been able to have a night off and just have a good time!
Orange: I can't wait either! It will be so much fun!

#### Day 10
###### Email - McLure
It looks like we may have a chance to kill this rebellion before it even starts. Thanks to your information, we have a company of troops surrounding the mill, ready for the rebels to show up so we can eradicate them like the weeds they are in the garden of Utopia. And it is all thanks to your work here. Without you, they could have escaped us. But you've personally taken the safety off of the rifle aimed for their vitals. Good work.
I will be watching over this operation personally, so just continue the good work until I am back.
###### Messages
Red: I know we shouldnt be {talking} about this while the bosses are meeting up, but what if they've pushed too quickly.
Blue: {They} are the {bosses} for a {reason}. We just have to trust them.
Red: I {hope} you are {right}. We can't throw away our shot at this.

#### Day 11
###### No Email
###### Messages
Blue: {Urgent}!
Blue: {Government} {snipers} found in {outskirts} of mill!
Blue: {Abort}!

#### Day 12
###### No Email
###### Messages
Blue: What happened to {careful}? Not being {followed}? What if we hadn't set advance scouts?
Red: {Us}? We were {careful}, just as we always are. You screwed it up, not us.

### Event 4
Rebels add Substitution cipher with known key.

#### Day 13
###### Email - McLure
I can't believe that someone I hand picked for the response team could have been irresponsible enough to be spotted by the rebels. They have been dealt with, but it's infuriating that we were so close, and then nothing.
Just to make things worse, they've upped their encipherment again. It looks like this one is based on a word instead of a straight match. Information is on your pinboard. If we have any information on a key for the cipher, that will also be put on your pinboard.
###### Messages
Red: We can't {afford} a {close call} like that again.
Red: We are {dropping off} the {radar} for a while.

#### Day 14
###### Email - McLure
There has been a lower volume of messages coming through recently. Don't be surprised if you have a day without any.
###### Messages
Orange: What are you going to wear?
Pink: I was thinking that nice red dress I got for my birthday. What do you think?
Orange: Perfect!

#### Day 15
###### Email - McLure
Just a reminder that the contents of the office fridge is not "fair game". Please only take things that you put in yourself. Also, due to "biological hazards", the fridge will be emptied every afternoon. Anything left will be discarded. Thankyou for your co-operation.
###### No messages

#### Day 16
###### No email
###### Messages
White: AZZA or Cae1
MCLURE
IS
____

#### Day 17
###### No email
###### No messages

### Event 5
Reports of rebels keylogging Government computers. Temporary output change to random Substitution cipher.

#### Day 18
###### Email - McLure
Urgent. There have been reports of rebel hacking into our systems. We believe these are just rumours, but in the interest of security, please encipher any output you may have using the substitution cipher with key "Utopian". Thankyou.
###### Messages
Red: We are {six hours} from {your base}. If you come now, we can meet up before they have a chance to respond.
Red: We have {hacked into} the {government systems}, and tripped some alarms to throw them off.
Red: We don't have {much time}. {Hurry}

#### Day 19
###### Email - McLure
I can't believe that they got past us again. Everyone involved must report to me today.
Everyone must up their game starting today. If they get away again, there will be consequences.
For today, use key "Interdiction" for any transcriptions.
###### Messages
Blue: Was there anything {useful} in what we gave you?
Red: Yes. {Thankyou} for coming on such short notice.
Red: We will {definitely} be able to {use this}.

### Event 6
Computers secured from keylogging.

#### Day 20
###### Email - McLure
Computer system has been verified to be secured from rebel hacks and as such, your transcriptions don't need to be enciphered anymore.
In other news, a paper has been found at the meeting site with detailed blueprints. R&D are trying to decipher this now.
###### Messages
Green: He's still doing {well}.
Green: {Maybe} could've {decoded} the last meeting message quicker, so we could get there in time, but that's not important.


### Event 7
Rebels add Numeric Encoding.

#### Day 21
###### Email - McLure
Just to keep you on your toes, there appears to be a new cipher they are using. Details are on your pinboard. Other than that, it's just business as usual.
###### Messages
Red: The {end} is {coming}. {Revenge} is {coming}. {Soon}
Blue: {How soon}?
Reed: {Give} us a {week} and we can make our move.

### Event 8
Rebels combine Numeric with existing ciphers.

#### Day 22
###### Email - McLure
Just a heads up, sometimes there is more than one cipher combined. If you are pretty sure you know what you know what it is, but it doesn't seem to still make sense, maybe there is a second cipher.
###### Messages
Blue: I hope you {know} what you are {doing}. We can't afford you to ruin this chance.
Blue: We won't get {another chance}. {{numeric}}

#### Day 23
###### Email - Carmel
McLure. You know what we want. You have one week before our hands are forced.
###### Messages
Red: We have sent the {threat}. It's time.
Red: In {one week}, we will meet our {goal}, or we will destroy them.
Blue: {Already}? We still have so much preparation to do.

#### Day 24
###### Email - McLure
I know you all know about the threats that have been made against our great Utopia. I just want to ensure you all, there is nothing that a group of insects like these rebels could do against us. We will be strong.
###### No Message

#### Day 25
###### Email - McLure
I have heard rumours around the office of weakness in the infrastructure. That the rebels have any chance of even making a blemish on Utopia. They do not. We have power, resources, an army. They have a bunch of maligned ideals, and people who are too weak to fight for them.
###### Messages
Blue: I {wonder} if this will {work}. Can we get inside their heads?
Blue: Can we {actually} {do it}.

#### Day 26
###### Email - McLure
We will not give anything. We are power. We are greatness. We will not stoop to the level of these terrorists by even acknowledging their threats. They are empty threats. We will not negotiate. We will overrule them.
###### Messages
White: <Azza or Cae1>
honorable___
patriotic___
heroic___

#### Day 27
###### Email - McLure
Even if these rebels did have some move to make against us, it would be treated as an act against Utopia. A declaration of war. A war that we outclass them in in every way. A war we will dedicate ourselves to if only to prove a point.
###### Messages
White: <Azza or Cae1>
stubborn___
dangerous___
warmonger___

### Event 10
MCLURE IS DEAD

#### Day 28
###### Email - McLure
But no matter what. We will win. This grand country will always come out on top. Rebel scum such as these stand no chance against the might of Utopia. We will always stay one step ahead.
###### Messages
White: Plaintext
CRUEL___
EVIL___
...
DEAD
Gunshot.
Black

#### Day 29
###### Email - Carmel
We know you were the one deciphering our messages. We could use someone like you on our side. Your target will have much stronger ciphers than we have been able to use, so if you don't know what ciphers may be coming up, you probably won't be much use to us anyway. If you think you can handle it, you would be contributing to a cause much greater than yourself. We cannot give you long to make your decision, but we hope you make the right one.
###### Messages
Pink: Did you see the news? A govenment officer was killed yesterday?
Orange: But no body, no witnesses? Suspicious as hell!
Pink: There was just that casette tape left at the scene. Sounds pretty gruesome though.

#### Day 30
###### Email - Thompson
Hello everyone.
I am Gary Thompson. Due to recent events, I will be taking over from where James McLure was.  Things might be a little hectic until the murder investigation finishes, but that will not stop us from finding and destroying the rebel sects.
###### No messages

#### Day 31
###### Email - Carmel
You must make your decision. We need your help. Don't let us down.
Choose between rebel path or govt path.

If choose rebels:
I knew you would come to the right answer. We will send you all the details. You start tomorrow. Welcome to the Uprising.

If choose govt:
I am sorry to hear that. I suppose you will just have to die with the rest of the swine who are trying to choke the life out of this once-great country. Goodbye Traitor. Rot in hell.

#### Day 1g
### Event 11
Rebels start ciphering whole messages.

###### Email - Thompson
We don't know what the rebels are up to, but we must find out.
Keep at it. I've heard great things about you.
###### Messages
Red: The decrypter refused our offer. It is a shame, but it is what it is.
Red: We don't need them anyway.

### Event 13
Rebels start using Rail cipher with known height.

#### Day 2g
###### Email - Thompson
This is new. I have not seen this cipher before. There was a note left in McLure's desk that might apply, but I can't know for sure. They must be really getting ready to do something big if they are breaking out these ciphers.
###### Messages
Blue: How far away from being ready are you?
Red: The weapons are being prepared as we speak.
Red: We will be ready before they can stop us.

#### Day 3g
###### Email - Carmel
Your fate has been sealed. If you cannot accomodate us, you will accommodate nothing. Your country is finished. You have abused us, the people, for too long. It is time to take back what you took from us.
###### Messages
Blue: Are you sure this is the best idea?
Red: Once we start this, it will be a matter of hours before the country is crippled.
The only way they will listen to us is if we give them no other choice.

### Event 14
Rebels start using columnar cipher with known key.

#### Day 4g
###### Email - Thompson
We have to figure our what they are up to. This is another new cipher. Our spy delivered a note to us earlier, that I can only assume applies to it. Good luck.
###### Messages
Red: We have found a weakpoint. It will take another day or two to fully charge though.
Blue: Hopefully the amount of power we are drawing won't draw too much attention

#### Day 5g
###### No email
###### Messages
Red: Tomorrow, we can take down their systems for good. It is as good as done.
Good bye tyrants.


#### Day 6g
###### Email - Kate
You don't have much time. I've found the disable code for their weapon. I saw them encipher it used the rail fence, but they destroyed it before I could see how many lines there were in it. Please solve it. You are our only hope.
###### Message
Green: The Clumsier Veil will hide us.

#### Day 7g
###### Email - Thompson
Congratulations to all. Thanks to the diligent work of all of you, the rebels have been disbanded, and Utopia stands, stronger than ever. After the weapon was stopped, Agent Burnell led our troops to the base of the rebels, so we could stop their scheming. Utopia is safe. Well done everyone.






#### Day 1r
### Event 11
Govt starts ciphering whole messages.

###### Email - Carmel
I'm glad to see you decided to join us, but I'm afraid we don't have time for pleasantries. We have a plan on how we can take down the Govt, but the longer we leave it the longer they have to find a solution. We can explain why we are doing all this later, but since you are here, you must believe that already. The messages you are deciphering here will be more difficult than what you have been deciphering from us, but I'm sure you can do it.
###### Messages
Green: Where is the {new guy}? He's only been here a month.
Green: Is he already missing work? Some people just don't care enough for Utopia.


### Event 13
Govt starts using Rail cipher with known height.
Govt starts using columnar cipher with known key.

#### Day 2r
###### Email - Lindsay
I am Linsday Glynne. I am the now second-best decipherer amongst the rebels. While you may be more versed in decipher in general though, nothing beats first hand field experience. Here are my notes on ciphers the government has been known to use recently. You should still have some information on the keys that they were using. It will take a couple of days before they can change the keys, and a couple more before they can change the length, so use that to our advantage until then.
###### Messages
Green: Burnell is back? She said she saw the new guy at rebel base? She can't stay there if he might recognise her...

#### Day 3r
###### Email - Thompson
I am very disappointed to hear that you have abandoned Utopia in order to "make a difference to the forgotten people". I just hope you realise what you've given up before your entire rebellion is squashed.
###### Messages
Green: We need to change all of our keys, all of our key lengths. Everything.


### Event 16
Govt start using columnar cipher with unknown key but known length.

#### Day 4r
###### Email - Carmel
Just for the record, we didn't want to take this to this length. We only wanted the rights that we once had. Since the Utopia Project began, everyone who wasn't in the center of it all has been being shafted. It started off simple. Police would respond more slowly on the outskirts. Then they stopped coming. Then they stopped answering our calls altogether. Family from elsewhere couldn't come visit, because they revoked the ability to check them from all the airports outside of the center. Next thing we knew, we couldn't vote on anything the government did, while everyone in the middle of the project could. People were dying, and we had no running hospitals, because the funding was only passed for Central Utopia hospitals. We needed to do something drastic...
###### Messages
Green: All the keys have been changed. All communication should be safe from the rebels now.

#### Day 5r
###### Email - Tom
The worst bit is, Red's story is pleasant compared to how they treated some of us. Everyone in the Exiles was "too dangerous" to live in Utopia. Anyone who even showed the slightest inkling of resisting leadership was branded a traiter and sentenced to "death". Except instead of death, they meant involuntary medical testing of whatever they felt like. They love to boast that the found a cure for cancer, but that "cure" killed tens of thousands of people. Some of us managed to escape, but not many. And there was nothing we could do to get these experiences to the people inside, because if there was any trace that we were alive, we would be hunted down. We had no food, living in run down building left over from the old world, just tryingour best to survive. But we are all that is left now.
###### Messages
Green: Start using the emergency ciphers. He can't be allowed to find our weak point.


### Event 17
Rebels start using substitution cipher on single words, then another cipher on whole string.

#### Day 6r
###### Email - Carmel
Our systems are almost ready to hack in and take down Utopia. We just need to figure out where to target it.
The government has started using multiple ciphers on a single message. We are so close. We can't fail now.
###### Messages
Green: We have reports of rebel activity from the east coast. Should we send scouts?
Green: Yes. Shut them down, before they find the backdoor.

#### Day 7r
###### Email - Sebastian
I know you probably don't know who I am. I was "dismissed" from the ARD response team after McLure needed a scapegoat for the meeting weeks ago falling apart. I just wanted to thank you for finding out about these rebels. I wouldn't have had anywhere else to turn. Dismissal in Utopia isn't just a matter of find another job. It's exile or death.
I might be able to help you find the codephrase and target to shut down their systems.
###### Messages
Green: Tomorrow, we strike at midnight.

#### Day 8r
###### Email - Tom
Sebastian has helped us find the passcode. We have no idea about how it is ciphered though. This is it. You can do it. We all believe in you.
###### Messages
Blue: The {clumsier veil} shall be our {downfall} {{Rail fence 3-6}}



### Event 18
Rebels start using rail cipher with unknown height.

### Event 19
Rebels start chaining two complex ciphers on a whole string, with known keys.

### Event 20
Rebels chain two complex ciphers on a whole string, with only first key known.

### Event 21
McLure?





