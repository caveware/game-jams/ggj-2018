const path = require("path")
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const webpack = require("webpack")

const mkpath = function (value) {
	return path.join(__dirname, value)
}

module.exports = {
	devServer: {
		compress: true,
		contentBase: __dirname,
		inline: true,
		port: 8642,
		watchOptions: {
			aggregateTimout: 300,
			ignored: /node_modules/,
			poll: true
		}
	},
	resolve: {
		alias: {
			pixi: mkpath('node_modules/phaser-ce/build/custom/pixi.js'),
			phaser: mkpath('node_modules/phaser-ce/build/custom/phaser-split.js'),
			p2: mkpath('node_modules/phaser-ce/build/custom/p2.js')
		},
		extensions: [ ".js", ".ts" ]
	},
	devtool: "source-map",
	entry: mkpath("app/entry.ts"),
	module: {
		rules: [
			{ test: /pixi\.js$/, loader: 'expose-loader?PIXI' },
			{ test: /phaser-split\.js$/, loader: 'expose-loader?Phaser' },
			{ test: /p2\.js$/, loader: 'expose-loader?p2' },
			{ test: /\.ts$/, loader: 'ts-loader', exclude: '/node_modules/' }
		]
	},
	output: {
		filename: "bundle.js",
		path: __dirname
	},
	plugins: [
		// new UglifyJsPlugin()
	]
}